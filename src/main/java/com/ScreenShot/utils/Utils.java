package com.ScreenShot.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import com.MobileBaseClasses.MobileDeviceController;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;


/**
 * 
 * This is utils class, Which has got few functions which are commonly used across the project.
 * For example, For taking screen shots of the error, Dropdown selection on any page.
 *
 */
public class Utils {

	public String takeScreenshot(AppiumDriver<MobileElement> driver, String screenshotName) throws IOException {
				
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");
		File file = new File(df.format(date));
		
			TakesScreenshot ts = (TakesScreenshot) MobileDeviceController.driver;
			File source = (File) ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("./screenshots/"+ file + "_" + screenshotName + ".png");
			//File finalDestination =new File(destination);
			//FileUtils.copyFile(source,finalDestination);
			
			FileUtils.copyFile(source, new File("./screenshots/" +screenshotName+df+".png"));

			System.out.println("Screenshot taken");
			return destination;
								
	}
	

}
