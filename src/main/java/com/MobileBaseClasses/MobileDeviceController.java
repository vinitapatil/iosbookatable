package com.MobileBaseClasses;
/**
 * @author Vinita Patil
 */
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.ShakesDevice;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MobileDeviceController {
	public static OS executionOS = OS.IOS;
	public enum OS {   
        ANDROID,
        IOS
    }
	public static MobileDeviceController instance = new MobileDeviceController();
    public static AppiumDriver<MobileElement> driver;
    public DesiredCapabilities capabilities;
    public static String accessKey = "jXhj9WZYaA69QjJ3bGFU";
	public static String userName = "londonqa1";	
	//curl -u londonqa1:jXhj9WZYaA69QjJ3bGFU -X POST https://api-cloud.browserstack.com/app-automate/upload -F file=@/bitrise/src/fastlane/build/app-dev-debug.apk -F 'data={"custom_id": "MyApp"}'

	@SuppressWarnings("deprecation")
	@BeforeTest
 	public void start() throws MalformedURLException, InterruptedException {
        if (driver != null) {
            return;
        }
        switch(executionOS){
        case ANDROID:
        	
        	capabilities = new DesiredCapabilities();        	
        	 capabilities.setCapability("automationName", "appium");   
        	
        	capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("deviceName", "Android device");
            //capabilities.setCapability("language", "de");
            //capabilities.setCapability("locale", "De");
            //capabilities.setCapability("locale", "en-GB");
            capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.bookatable.android.debug");
            //capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.android.packageinstaller.permission.ui.GrantPermissionsActivity");

           capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.bookatable.android.uimodules.splash.SplashActivity");
            driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
         
            System.out.println("ANDROID IS RUNNING");
          
          /*   capabilities.setCapability("device", "Galaxy S9");
            capabilities.setCapability("os_version", "8.0");
            capabilities.setCapability("real_mobile", "true");
            //capabilities.setCapability("locale", "en-GB");
            capabilities.setCapability("app", "londonqa1/MyApp");    
            //capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.bookatable.android.debug");
           // capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.android.packageinstaller.permission.ui.GrantPermissionsActivity");

            driver = new AndroidDriver<MobileElement>(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), capabilities);
               */
            break;
      
        case IOS:
        	
			capabilities = new DesiredCapabilities();
			
			/*   capabilities.setCapability("automationName", "XCUITest");					
			capabilities.setCapability("platformName", "iOS");
            capabilities.setCapability("deviceName", "iPhone 6");
            capabilities.setCapability("platformVersion", "10.3.3");
            capabilities.setCapability("udid", "2e10c01c342b5feccd5b369b24451d322a6a0dc8");
            //capabilities.setCapability("language","de");
           // capabilities.setCapability("locale", "de-de");
            capabilities.setCapability("bundleId", "com.bookatable.apps.michelinrestaurants");
            driver = new IOSDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
         */ 
            System.out.println("IOS IS RUNNING");
     	 capabilities.setCapability("automationName", "XCUITest");
			capabilities.setCapability("device", "iPhone 8 Plus");
			capabilities.setCapability("realMobile", "true");
			capabilities.setCapability("os_version", "11.0");
			capabilities.setCapability("browserstack.debug", "true");	
			capabilities.setCapability("browserstack.gpsLocation", "51.5074,0.1278");
			capabilities.setCapability("language","de");
            capabilities.setCapability("locale", "de-de");
            capabilities.setCapability("takesScreenshot", "false"); 
            
            capabilities.setCapability("bundleId", "com.bookatable.apps.michelinrestaurants");
			capabilities.setCapability("app", "londonqa1/MyApp");

	        driver = new IOSDriver<MobileElement>(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), capabilities);
	          
        
			
            break;
    }
    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);   
	}
	/*@Test
	public void mybooking()
	
	{
				
		driver.findElement(By.id("Bookings")).click();		

		driver.findElement(By.xpath("//XCUIElementTypeSegmentedControl[1]/XCUIElementTypeButton[2]")).click();
		
		driver.findElement(By.xpath("//XCUIElementTypeSegmentedControl[1]/XCUIElementTypeButton[1]")).click();
		
		System.out.println(driver.findElement(By.xpath("//XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]")).getText());
		
		System.out.println(driver.findElement(By.xpath("//XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]")).getText());

		driver.findElement(By.id("rightDisclosure")).click();
		
		System.out.println(driver.findElement(By.xpath("//XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]")).getText());

		System.out.println("Person :"+driver.findElement(By.xpath("//XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]")).getText());

		System.out.println("Day :"+driver.findElement(By.xpath("//XCUIElementTypeOther[2]/XCUIElementTypeStaticText[2]")).getText());

		System.out.println("Time :"+driver.findElement(By.xpath("//XCUIElementTypeOther[3]/XCUIElementTypeStaticText[2]")).getText());

		
	}*/
	
}
