package com.PageObjects;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Vinita Patil!
 *
 */
public class RestaurantNearMePageAndroid extends RestaurantNearMePage {
	private AndroidDriver<MobileElement> driver;

	public RestaurantNearMePageAndroid(AppiumDriver<MobileElement> driver) {
		 this.driver = (AndroidDriver<MobileElement>) driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);

	}
	@AndroidFindBy(id = "com.bookatable.android.debug:id/error_text_navigation_link")
	public MobileElement try_again_button;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/error_text_header")
	public MobileElement we_cannot_find_restaurant_text;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/txt_restaurant_near_me")
	public MobileElement search_tab_text;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/error_text")
	public MobileElement do_not_worry_text;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/error_text_navigation_link")
	public MobileElement Cancel_button;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/edit_search_view")
	public MobileElement search_Texbox;
	
	@AndroidFindBy(id = "android.widget.FrameLayout")
	public MobileElement frame_Layout;

	@AndroidFindBy(id = "android.widget.ImageView")
	public MobileElement image_view;

	@AndroidFindBy(id = "com.bookatable.android.debug:id/txt_restaurant_name")
	public MobileElement txt_restaurant_name;

	@AndroidFindBy(id = "com.bookatable.android.debug:id/txt_city_name")
	public MobileElement txt_city_name;

	@AndroidFindBy(id = "com.bookatable.android.debug:id/root_distance")
	public MobileElement root_distance;

	@AndroidFindBy(id = "com.bookatable.android.debug:id/edit_search_view")
	public MobileElement edit_search_view;

	@AndroidFindBy(id = "com.bookatable.android.debug:id/search_src_text")
	public MobileElement search_src_text;

	@AndroidFindBy(id = "com.bookatable.android.debug:id/txt_restaurant_near_me")
	public MobileElement restaurant_near_me;
	
	@AndroidFindBy(className = "android.widget.LinearLayout")
	public MobileElement location_title;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/txt_restaurant_name")
	public MobileElement name_dropdown_view;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/txt_restaurant_name")
	public List<MobileElement> get_dropdown_count;
	//com.bookatable.android.debug:id/img_search_type
	@AndroidFindBy(id = "com.bookatable.android.debug:id/txt_restaurant_name")
	public List<MobileElement> get_restaurant_name_dropdown;	
	
	@AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.TextView")
	public MobileElement select_First_Record_For_Location_Search;

	@AndroidFindBy(id = "com.bookatable.android.debug:id/txt_restaurant_name")
	public MobileElement restaurant_name_listView;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/txt_city_name")
	public MobileElement restaurant_address_listView;
	
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[@index='0']")
	public MobileElement list_frame_Layout;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/error_text_header")
	public MobileElement locationServiceErrorHeader;
	
	@AndroidFindBy(id = "com.bookatable.android.debug:id/error_text")
	public MobileElement locationServiceErrorDesc;
		
	@AndroidFindBy(id = "com.bookatable.android.debug:id/error_text_navigation_link")
	public MobileElement locationServiceNavigationLink;
	
	@AndroidFindBy(id = "com.android.settings:id/switch_widget")
	public MobileElement deviceLocationService;
	
	public boolean frameContainer() {
		try {
			frame_Layout.click();
			return true;
		} catch (Exception e) {
			System.out.println("Add New button was not found");
			return false;
		}
	}

	public boolean searchTextboxEdit() {
		try {
			Thread.sleep(1000);
			edit_search_view.click();
			return true;
		} catch (Exception e) {
			System.out.println("Search Edit Textbox  was not found");
			return false;
		}
	}

	public MobileElement searchTextbox() {

		return search_src_text;
	}

	public boolean searchTextboxClear() {

		try {
			search_src_text.clear();
			System.out.println("Search Textbox has been cleared");
			return true;
		} catch (Exception e) {
			System.out.println("Search Textbox was not found");
			return false;
		}
	}

	public boolean imageViewContainer() {
		try {
			image_view.click();
			System.out.println("Add New Button has been clicked");
			return true;
		} catch (Exception e) {
			System.out.println("Add New button was not found");
			return false;
		}
	}

	public void HideKeyboard() {
		//driver.pressKeyCode(AndroidKeyCode.);
		 driver.hideKeyboard();
		//driver.executeScript("mobile:keyevent", "keycode:66");
	}

	@Override
	public boolean tryAgainButton() {
		// TODO Auto-generated method stub
		return false;
	}

	

	@Override
	public void getlocationNamefromDropDown() {
		// TODO Auto-generated method stub

	}

	@Override
	public int getDropdownCount() {
		int dropdowncount = get_dropdown_count.size();
		return dropdowncount;
	}

	@Override
	public int getLocationCountDropdownList() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean selectFirstRestaurantFromTheList() {
		list_frame_Layout.click();
		return true;
	}

	@Override
	public boolean selectFirstRecordFromtheSearchList() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tapOnRestaurantNearMe() {
		try {
			restaurant_near_me.click();
			return true;
		} catch (Exception e) {
			System.out.println("Restaurant Near me button was not found.");
			return false;
		}
	}

	@Override
	public boolean tapOnClearTextButton() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tapOnCancelButton() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void txtRestaurantName() {

		for (int i = 0; i < get_restaurant_name_dropdown.size(); i++) {
			System.out.println(get_restaurant_name_dropdown.get(i).getText());
		}
	}
	
	@Override
	public String getrestaurantNameListView() {
		return restaurant_name_listView.getText();
	}

	@Override
	public String getrestaurantAddressListView() {
		return restaurant_address_listView.getText();
	}

	@Override
	public boolean selectFirstRecordForLocationSearch() {
		select_First_Record_For_Location_Search.click();
		return true;
	}

	@Override
	public String getTextTryAgainButton() {
		try 
		{
			return try_again_button.getText();
			
		}
		catch (Exception e) 
		{
			System.out.println("Try Again button was not found.");
			return null;
		}
	}

	@Override
	public String getTextRestaurantNearMe() {
		try 
		{
			return search_Texbox.getText();
		}
		catch (Exception e) 
		{
			System.out.println("Restaurant Near me text was not found.");
			return null;
		}
	}

	@Override
	public String getTextWeCannotFindRestaurantText() {
		try 
		{
			return we_cannot_find_restaurant_text.getText();
		}
		catch (Exception e) 
		{
			System.out.println("We Cannot Find Restaurant text was not found.");
			return null;
		}
	}

	@Override
	public String getTextForDoNotWorry() {
		try 
		{
			return do_not_worry_text.getText();
		}
		catch (Exception e) 
		{
			System.out.println("Do Not Worry text was not found.");
			return null;
		}
	}

	@Override
	public String getTextForSearchTab() {
		try 
		{
			return search_tab_text.getAttribute("text");
			
		}
		catch (Exception e) 
		{
			System.out.println("Search Tab Text was not found.");
			return null;
		}
	}
	
	@Override
	public String getTextLocationServiceErrorHeader() {
		try 
		{
			return locationServiceErrorHeader.getText(); 
		}
		catch (Exception e) 
		{
			System.out.println("Location Service Error Header text was not found.");
			return null;
		}
	}
	@Override
	public String getTextLocationServiceErrorDesc() {
		try 
		{
			return locationServiceErrorDesc.getText(); 

		}
		catch (Exception e) 
		{
			System.out.println("Location Service Error Description text was not found.");
			return null;
		}
	}
	@Override
	public String getTextLocationServiceNavigationLink() {
		try 
		{
			return locationServiceNavigationLink.getText(); 

		}
		catch (Exception e) 
		{
			System.out.println("Location Service Navigation Link text was not found.");
			return null;
		}
	}
	@Override
	public boolean tapOnLocationServiceNavigationLink() {
		try 
		{
			locationServiceNavigationLink.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Location Service Navigation Link was not found.");
			return false;
		}
	}
	
	@Override
	public boolean tapOnDeviceLocationService() {
		try 
		{
			deviceLocationService.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Device Location Service toggle button was not found.");
			return false;
		}
	}
	@Override
	public boolean tapOnDeviceBackButton() {
		driver.pressKeyCode(AndroidKeyCode.BACK);

		return true;
			
	}
	@Override
	public String getTextForBookingsTab() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean selectFirstRecordForRestaurantSearch() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getTextForCancelButton() {
		// TODO Auto-generated method stub
		return null;
	}

}
