package com.PageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BDAScreenAndroid extends BDAScreen{
	private AndroidDriver<MobileElement> driver;
	private WebDriverWait waitDriver;

	public BDAScreenAndroid(AppiumDriver<MobileElement> driver) {
		this.driver = (AndroidDriver<MobileElement>) driver;
		this.waitDriver = new WebDriverWait(this.driver, 20);

		}
	public By people_dropdown = By.className("android.view.View");

	public By people_dropdown_List = By.className("android.view.View");
		
	public By meal_dropdown = By.id("Dinner");
	
	public By meal_dropdown_list = By.xpath("");

	public By dropdown_done_button = By.id("");
	
	public By select_calendar_date = By.xpath("//android.view.View[@text='7']");

	//public By select_calendar_date = By.xpath("//XCUIElementTypeStaticText[contains(@value,'31')]");

	public By tap_on_book_button = By.xpath("//android.view.View[@resource-id='lnkPromoId_0']");

	public By select_time_slot = By.className("android.view.View");

	public By textbox = By.className("android.widget.EditText");

	public By enter_first_name = By.id("settingsFirstname");

	public By enter_last_name = By.id("settingsLastname");

	public By enter_email = By.id("settingsEmail");

	public By enter_phone_number = By.id("settingsPhonenumber");

	public By tap_continue = By.xpath("//android.view.View[@text='Continue']");
		
	public By tap_on_close = By.id("com.bookatable.android.debug:id/action_close");
	
	public By book_now_final_booking = By.xpath("//android.view.View[0]");
	
	public By back_on_final_booking = By.xpath("");

	public By done_on_booking_confirmed = By.id("");
	
	@Override
	public boolean tapOnPeopleDropdown() 
	{
		try 
		{
			MobileElement peopleDropdown = driver.findElement(people_dropdown);
			peopleDropdown.click();
			
			//System.out.println("Select People Dropdown has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Select People Dropdown was not found");
			return false;
		}
	}
	@Override
	public boolean tapOnPeopleDropdownList() 
	{
		try 
		{			

			List <MobileElement> peopleDropdownList = driver.findElements(people_dropdown_List);
			peopleDropdownList.get(2).sendKeys("3 people");			
			
			//System.out.println("Select People Dropdown has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Select People Dropdown was not found");
			return false;
		}
	}
	
	@Override
	public boolean tapOnMealDropdown() 
	{
		try 
		{
			MobileElement peopleDropdown = driver.findElement(meal_dropdown);
			peopleDropdown.click();
			List <MobileElement> mealDropdown = driver.findElements(meal_dropdown);
			mealDropdown.get(1).click();
			//System.out.println("Select Meal Dropdown has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Select Meal Dropdown was not found");
			return false;
		}
	}
	
	@Override
	public boolean selectCalendarDate() 
	{
		try 
		{
			waitDriver.until(ExpectedConditions.elementToBeClickable(select_calendar_date));

			MobileElement calendar_date = driver.findElement(select_calendar_date);
			calendar_date.click();
			//System.out.println("date has been selected");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Calender event was not found");
			return false;
		}
	}
	
	@Override
	public boolean tapOnBookButton() 
	{
		try 
		{
			MobileElement book_button = driver.findElement(tap_on_book_button);
			book_button.click();
			//System.out.println("Book button has been Clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Book button was not found");
			return false;
		}
	}
	
	@Override
	public boolean selectTimeSlot() 
	{
		try 
		{
			waitDriver.until(ExpectedConditions.elementToBeClickable(select_time_slot));

			MobileElement time_slot = driver.findElement(select_time_slot);
			time_slot.click();
			//System.out.println("Time Slot has been Selected");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Time Slot has not been clicked");
			return false;
		}
	}
	
	@Override
	public boolean entertextbox() 
	{
		
			List<MobileElement> text_box= driver.findElements(textbox);
				
					text_box.get(0).sendKeys("first name");	
						System.out.println("Diner has entered First name : "+text_box.get(0).getText());

						text_box.get(1).sendKeys("Last name");
							System.out.println("Diner has entered Last name : "+text_box.get(1).getText());
								
							text_box.get(2).sendKeys("vinita.patil@capgemini.com");
									System.out.println("Diner has entered Email : "+text_box.get(2).getText());
								
									text_box.get(3).sendKeys("0123456789");
										System.out.println("Diner has enteredPhone Number : "+text_box.get(3).getText());
								
										return true;				
	}
		
@Override
public boolean tapOn_Close() 
{
	try 
	{
		MobileElement close = driver.findElement(tap_on_close);
		close.click();
		//System.out.println("Close button has been clicked");
		return true;
	}
	catch (Exception e) 
	{
		System.out.println("Close button was not found");
		return false;
	}
}


	@Override
	public boolean tapOn_Continue_button() 
	{
		try 
		{
			MobileElement continue_element = driver.findElement(tap_continue);
			continue_element.click();
			//System.out.println("Continue button has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Continue button was not found");
			return false;
		}
	}
	@Override
	public boolean tapOn_Booking_Now_Confirm_button() 
	{
		try 
		{
			MobileElement book_now_final_booking_element = driver.findElement(book_now_final_booking);
			book_now_final_booking_element.click();
			//System.out.println("Booking Now button has been clicked on confirm booking.");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Booking Now button was not found on confirm booking.");
			return false;
		}
	}
	
	@Override
	public boolean tapOn_done_on_booking_confirmed() 
	{
		try 
		{
			MobileElement done_on_booking_confirmed_element = driver.findElement(done_on_booking_confirmed);
			done_on_booking_confirmed_element.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("DONE button was not found on confirm booking.");
			return false;
		}
	}
	
}
