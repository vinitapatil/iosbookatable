package com.PageObjects;

public abstract class MyBookingPage {

	public abstract boolean tapOnSearchTab();

	public abstract boolean tapOnBookingsTab();

	public abstract boolean tapOnCancelBookingsButton();

	public abstract boolean getTimeTextInDetailsPage();

	public abstract boolean getDateTextInDetailsPage();

	public abstract boolean getPersonTextInDetailsPage();

	public abstract boolean getRestaurantNameInDetailsPage();

	public abstract boolean tapOnNextArrow();

	public abstract boolean getBookedRestaurantDetails();

	public abstract boolean getBookedRestaurantName();

	public abstract boolean selectFirstBookingRecord();

	public abstract boolean tapOnPastTab();

	public abstract boolean tapOnUpcomingTab();

	public abstract boolean tapOnFindARestaurantButton();
}
