package com.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Vinita Patil!
 *
 */
public class SplashScreenAndroid extends SplashScreen
{
	private AndroidDriver<MobileElement> driver;
	
	public SplashScreenAndroid(AppiumDriver<MobileElement> driver) {
		this.driver = (AndroidDriver<MobileElement>) driver;

		}
		
	  public By permission_message = By.id("com.android.packageinstaller:id/permission_message");

	  public By permission_allow_button = By.id("com.android.packageinstaller:id/permission_allow_button");
	
	  public By permission_deny_button = By.id("com.android.packageinstaller:id/permission_deny_button");
			
	  @Override
	  public String getPermissionMessage() {
			
			try {
			
				MobileElement permissionMessage = driver.findElement(permission_message);
				return permissionMessage.getText();
			
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Details Opening Hours Title Label is not found");
			}
			
			return null;
		}

	
	  @Override
	public boolean permissionAllowButton() 
	{
		try 
		{
			MobileElement permissionAllow_button = driver.findElement(permission_allow_button);
			permissionAllow_button.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Permission ALLOW button was not found");
			return false;
		}
	}
	  
	  @Override
	public boolean permissionDenyButton() 
	{
		try 
		{
			MobileElement permissionDeny_button = driver.findElement(permission_deny_button);
			permissionDeny_button.click();
			return true;

		}
		catch (Exception e) 
		{
			System.out.println("Permission DENY button was not found");
			return false;
		}
	}


	
}
