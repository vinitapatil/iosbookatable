package com.PageObjects;

import io.appium.java_client.MobileElement;

public abstract class RestaurantNearMePage{
		
	public abstract boolean imageViewContainer();
	
	public abstract MobileElement searchTextbox(); 

	public abstract void txtRestaurantName();

	public abstract void HideKeyboard();
	
	public abstract boolean tryAgainButton();

	public abstract int getLocationCountDropdownList();

	public abstract boolean selectFirstRestaurantFromTheList();

	public abstract void getlocationNamefromDropDown();

	public abstract String getTextTryAgainButton();

	public abstract int getDropdownCount();

	public abstract boolean selectFirstRecordFromtheSearchList();

	public abstract boolean tapOnClearTextButton();

	public abstract boolean tapOnCancelButton();
	
	public abstract String getrestaurantNameListView();

	public abstract String getrestaurantAddressListView();

	public abstract boolean selectFirstRecordForLocationSearch();

	public abstract String getTextRestaurantNearMe();

	public abstract String getTextWeCannotFindRestaurantText();

	public abstract String getTextForDoNotWorry();

	public abstract String getTextForSearchTab();

	public abstract String getTextForBookingsTab();

	public abstract String getTextForCancelButton();

	public abstract boolean selectFirstRecordForRestaurantSearch();

	public abstract boolean searchTextboxEdit();

	public abstract String getTextLocationServiceErrorHeader();

	public abstract String getTextLocationServiceErrorDesc();

	public abstract String getTextLocationServiceNavigationLink();

	public abstract boolean tapOnLocationServiceNavigationLink();

	public abstract boolean tapOnRestaurantNearMe();

	public abstract boolean tapOnDeviceLocationService();

	public abstract boolean tapOnDeviceBackButton();

	
}