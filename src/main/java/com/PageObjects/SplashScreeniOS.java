package com.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;

/**
 * Created by Vinita Patil!
 *
 */
public class SplashScreeniOS extends SplashScreen
{
	private IOSDriver<MobileElement> driver;
	
	public SplashScreeniOS(AppiumDriver<MobileElement> driver) {
		this.driver = (IOSDriver<MobileElement>) driver;
		
		}
	
	public By permission_message = By.xpath("//XCUIElementTypeAlert[contains(@name,'access your location while you use the app?')]");
	//public By permission_allow_button = By.xpath("//XCUIElementTypeButtonAlways Allow");

	public By permission_allow_button = By.id("Always Allow");
	//public By permission_allow_button = By.id("Allow");

	  //public By permission_allow_button = By.xpath("//XCUIElementTypeButton[@name='Allow']");
	
	public By permission_deny_button = By.xpath("//XCUIElementTypeButton[@name='Don’t Allow']");

	  
	
	@Override
	public String getPermissionMessage() {
		try {
			
			WebElement permissionMessage = driver.findElement(permission_message);
			return permissionMessage.getText();
		
		} catch (NoSuchElementException e) {
			
			System.out.println("Permission message element was not found.");
		}
		
		return null;
	}
	
	public boolean permissionAllowButton() 
	{
		try 
		{
			WebElement permissionAllow_button = driver.findElement(permission_allow_button);
			permissionAllow_button.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Permission ALLOW button was not found");
			return false;
		}
	}
	
	public boolean permissionDenyButton() 
	{
		try 
		{
			WebElement permissionDeny_button = driver.findElement(permission_deny_button);
			permissionDeny_button.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Permission DENY button was not found");
			return false;
		}
	}
	
}
