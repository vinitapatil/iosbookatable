package com.PageObjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

/**
 * Created by Vinita Patil!
 *
 */
public class RestaurantDetailsPageAndroid extends RestaurantDetailsPage {
	private AndroidDriver<MobileElement> driver;

	public RestaurantDetailsPageAndroid(AppiumDriver<MobileElement> driver) {
		this.driver = (AndroidDriver<MobileElement>) driver;
	}

	public By restaurant_name_label = By.id("com.bookatable.android.debug:id/restaurant_name");

	public By restaurant_details_opening_hours_title_label = By
			.id("com.bookatable.android.debug:id/txt_opening_hours_title");

	public By restaurant_details_opening_hours_value_label = By
			.id("com.bookatable.android.debug:id/opening_hours_text");

	public By restaurant_details_description_title_label = By
			.id("com.bookatable.android.debug:id/txt_description_title");

	public By restaurant_details_description_value_label = By
			.id("com.bookatable.android.debug:id/txt_description_value");

	public By book_Now_button = By.id("com.bookatable.android.debug:id/button_book_now");

	public By back_button = By.id("Navigate up");

	public By txt_location_title = By.id("com.bookatable.android.debug:id/txt_location_title");

	public By text_view_address_line_one = By.id("com.bookatable.android.debug:id/text_view_address_line_one");

	public By text_view_address_line_two = By.id("com.bookatable.android.debug:id/text_view_address_line_two");

	public By text_view_address_city = By.id("com.bookatable.android.debug:id/city");

	public By text_view_address_post_code = By.id("com.bookatable.android.debug:id/post_code");

	public By text_view_phone_title = By.id("com.bookatable.android.debug:id/text_view_phone_title");

	public By phone_number = By.id("com.bookatable.android.debug:id/phone_number");

	public By info_tab_button = By.xpath("//android.widget.TextView[@text='INFO']");

	public By contact_tab_button = By.xpath("//android.widget.TextView[@text='CONTACT']");

	public By contact_number_button = By.id("com.bookatable.android.debug:id/phone_number");

	public By cancle_call = By.id("Cancel");

	public By done_button = By.id("Navigate up");

	public By page_indicator = By.id("com.bookatable.android.debug:id/indicator");

	public By image_unavailable = By.id("Image unavailable");
	
	public By maps_view = By.xpath("//android.widget.RelativeLayout[2]");

	public By maps_view_blue_pin = By.className("android.view.View");

	public By text_on_maps_view_blue_pin = By.className("android.view.View");

	public By image_view = By.id("com.bookatable.android.debug:id/image");
	
	public By restaurant_details_phone_value_label = By.id("com.bookatable.android.debug:id/text_view_phone_title");

	@Override
	public String getRestaurantNameLabel() {

		try {
			MobileElement dialledNumberLabel = driver.findElement(restaurant_name_label);
			return dialledNumberLabel.getText();
		} catch (NoSuchElementException e) {
			System.out.println("Restaurant Name Label is not found");
		}

		return null;
	}

	@Override
	public String getRestaurantDetailsOpeningHoursTitleLabel() {

		try {

			WebElement dialledNumberLabel = driver.findElement(restaurant_details_opening_hours_title_label);
			return dialledNumberLabel.getText();

		} catch (NoSuchElementException e) {
			System.out.println("Restaurant Details Opening Hours Title Label is not found");
		}

		return null;
	}

	@Override
	public String getRestaurantDetailsOpeningHoursValueLabel() {

		try {
			MobileElement dialledNumberLabel = driver.findElement(restaurant_details_opening_hours_value_label);
			return dialledNumberLabel.getText();

		} catch (NoSuchElementException e) {
			System.out.println("Restaurant Details Opening Hours Value Label is not found");
		}

		return null;
	}

	@Override
	public String getRestaurantDetailsDescriptionTitleLabel() {

		try {
			MobileElement dialledNumberLabel = driver.findElement(restaurant_details_description_title_label);
			return dialledNumberLabel.getText();

		} catch (NoSuchElementException e) {
			System.out.println("Restaurant Details Description Title Label is not found");
		}

		return null;
	}

	@Override
	public String getRestaurantDetailsDescriptionValueLabel() {

		try {
			MobileElement dialledNumberLabel = driver.findElement(restaurant_details_description_value_label);
			return dialledNumberLabel.getText();

		} catch (NoSuchElementException e) {
			System.out.println("Restaurant Details Description Value Label is not found");
		}

		return null;
	}

	@Override
	public boolean tapOnBookNowButtonfunction() {
		// wait.until(ExpectedConditions.presenceOfElementLocated(book_Now_button));
		try {
			MobileElement bookNowbutton = driver.findElement(book_Now_button);
			bookNowbutton.click();
			return true;
		} catch (Exception e) {
			System.out.println("Book Now button was not found");
			return false;
		}
	}

	@Override
	public boolean tapOnInfoTab() {
		try 
		{
			MobileElement info_tab_element = driver.findElement(info_tab_button);
			info_tab_element.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Info Tab was not found");
			return false;
		}
	}

	@Override
	public boolean tapOnBackButton() {
		try 
		{
			MobileElement back_button_element = driver.findElement(back_button);
			back_button_element.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Back Now button was not found");
			return false;
		}
	}

	@Override
	public boolean tapOnDoneButton() {
		try 
		{
			MobileElement done_button_element = driver.findElement(done_button);
			done_button_element.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("DONE button was not found");
			return false;
		}
	}

	@Override
	public boolean tapOnMapView() {
		try 
		{
			MobileElement mapViewelement = driver.findElement(maps_view);
			mapViewelement.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Map View was not found");
			return false;
		}
	}
	 @Override
	  public boolean tapOnMapViewBluePin() 
		{
			try 
			{
				List<MobileElement> mapViewelement = driver.findElements(maps_view_blue_pin);
				
				mapViewelement.get(0).click();
				//mapViewelement.click();
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Map View Blue Pin was not found");
				return false;
			}
		}
	 @Override
	  public String getTextOnMapView() 
		{
			try 
			{
				List<MobileElement> mapViewelement = driver.findElements(maps_view_blue_pin);
				return mapViewelement.get(1).getAttribute("name");
			}
			catch (Exception e) 
			{
				System.out.println("Map View Blue Pin Restaurent Name was not found");
				return null;
			}			
		} 
	 @Override
	  public boolean swipeImages() 
		{
		  try 
			{
			  Dimension size = driver.manage().window().getSize();
			  System.out.println(size);
		        int anchor = (int) (size.height * 0.5);
		        int startPoint = (int) (size.width * 0.9);
		        int endPoint = (int) (size.width * 0.1);
		 
		        new TouchAction(driver).press(startPoint, anchor).waitAction(Duration.ofSeconds(4)).moveTo(endPoint, anchor).release().perform();
			  //TouchAction action = new TouchAction(driver); action.longPress(600, 500, Duration.ofSeconds(4)).moveTo(300, 500).release().perform();
			     
			   System.out.println("Swipe Successfully");
//http://appium.io/docs/en/writing-running-appium/touch-actions/
			   //https://stackoverflow.com/questions/23339742/how-to-perform-swipe-using-appium-in-java-for-android-native-app
			}
			   catch (Exception e) 
				{
					System.out.println("Image swipe was not successfull");
				}	
			return false;
					
		}
	 @Override
	  public String getTextImageUnavailable() 
		{
			try 
			{
				MobileElement image_unavailable_element = driver.findElement(image_unavailable);
				return image_unavailable_element.getText();
			}
			catch (Exception e) 
			{
				System.out.println("Map View Blue Pin Restaurent Name was not found");
				return null;
			}			
		} 
	 @Override
	  public boolean tapOnImageView() 
		{
			try 
			{
				MobileElement imageViewelement = driver.findElement(image_view);
				imageViewelement.click();
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Image View was not found");
				return false;
			}
		}
	 @Override
		public boolean tapOnContactTab() {
			try 
			{
				MobileElement contact_tab_element = driver.findElement(contact_tab_button);
				contact_tab_element.click();
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Contact Tab was not found");
				return false;
			}
		}
		@Override
		public String getTextForContactTab() {
			
			try 
			{
				MobileElement contact_tab_element = driver.findElement(contact_tab_button);
				String contact_tab_button_actual =contact_tab_element.getText();
				return contact_tab_button_actual;
			}
			catch (Exception e) 
			{
				System.out.println("Contact Tab was not found");
				return null;
			}
		}
	@Override
	public boolean tapOnCancelCall() {
		driver.pressKeyCode(AndroidKeyCode.BACK);

		return true;
			
	}
	@Override
	  public boolean swipeFromUpToBottom() 
		{
	
		try 
			{
			TouchAction action = new TouchAction(driver); action.longPress(500,600 , Duration.ofSeconds(4)).moveTo(500, 1600).release().perform();

			System.out.println("Swipe up was Successfully done.");
			}
			   catch (Exception e) 
				{
					System.out.println("swipe up was not successfull");
				}	
			return false;
				
		}
	@Override
	  public boolean swipeFromBottomToUp() 
		{		
		try {
			TouchAction action = new TouchAction(driver); action.longPress(500, 1600, Duration.ofSeconds(4)).moveTo(500, 600).release().perform();

		  System.out.println("Swipe down was Successfully done");
			}
			   catch (Exception e) 
				{
					System.out.println("swipe down was not successfull");
				}	
			return false;
					
		}


	@Override
	public String getContactText() {
		try {
			MobileElement dialledNumberLabelelement = driver.findElement(contact_number_button);
			
			return dialledNumberLabelelement.getText();
		} catch (NoSuchElementException e) {
			System.out.println("Restaurant Name Label is not found");
		}
		
		return null;
	}

	@Override
	public MobileElement tapOnPhoneNumber() {

		MobileElement dialledNumberLabelelement = driver.findElement(contact_number_button);
		return dialledNumberLabelelement;	
	}

	@Override
	public String getTextForInfoTab() {
		try 
		{
			WebElement info_tab_element = driver.findElement(info_tab_button);
			String info_tab_button_actual=info_tab_element.getText();
			return info_tab_button_actual;
		}
		catch (Exception e) 
		{
			System.out.println("Info Tab was not found");
			return null;
		}
	}

	@Override
	public String getTextForBookNowButton() {
		try 
		{
			WebElement book_now_button_element = driver.findElement(book_Now_button);
			return book_now_button_element.getText();
		}
		catch (Exception e) 
		{
			System.out.println("Book Now button was not found");
			return null;
		}
	}

	@Override
	public String getRestaurantDetailsPhoneValueLabel() {
		try {
			WebElement restaurant_details_phone_value_label_element = driver.findElement(restaurant_details_phone_value_label);
			return restaurant_details_phone_value_label_element.getText();
		
		} catch (NoSuchElementException e) {
			System.out.println("Restaurant Details Description Value Label is not found");
		}
		
		return null;
	}
}
