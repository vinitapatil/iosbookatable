package com.PageObjects;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;

public class BDAScreeniOS extends BDAScreen{
	
	private IOSDriver<MobileElement> driver;
	private WebDriverWait waitDriver;

	public BDAScreeniOS(AppiumDriver<MobileElement> driver) {
		this.driver = (IOSDriver<MobileElement>) driver;
		this.waitDriver = new WebDriverWait(this.driver, 20);
		
		}
	//((IOSElement)driver.findElements(By.className("UIAPickerWheel")).get(indexvalue)).sendKeys("12");
	
	public By people_dropdown = By.id("2 people");

	public By people_dropdown_List = By.className("XCUIElementTypeOther");
		
	public By meal_dropdown = By.id("Dinner");
	
	public By meal_dropdown_list = By.xpath("//XCUIElementTypeOther[@name='Select an option below:']");

	public By dropdown_done_button = By.id("22");
	
	public By select_calendar_date = By.xpath("//XCUIElementTypeOther[@name='main']/XCUIElementTypeOther[2]/XCUIElementTypeOther[8]/XCUIElementTypeOther[3]/XCUIElementTypeOther[5]");

	//public By select_calendar_date = By.xpath("//XCUIElementTypeStaticText[contains(@value,'31')]");

	public By tap_on_book_button = By.xpath("//XCUIElementTypeOther[@name='main']/XCUIElementTypeOther[2]/XCUIElementTypeOther[9]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther");

	public By select_time_slot = By.xpath("//XCUIElementTypeButton[@name='20:00']");

	public By textbox = By.className("XCUIElementTypeTextField");

	public By enter_first_name = By.xpath("//XCUIElementTypeTextField[@name='e.g. John, Ingrid etc.']");

	public By enter_last_name = By.xpath("//XCUIElementTypeTextField[@name='e.g. Smith, Patel etc.']");

	public By enter_email = By.xpath("//XCUIElementTypeTextField[@name='e.g. john@bookatable.com']");

	public By enter_phone_number = By.xpath("//XCUIElementTypeTextField[@name='e.g. 07000 000000']");

	public By tap_continue = By.xpath("//XCUIElementTypeButton[@name='Continue']");
		
	public By tap_on_close = By.xpath("//XCUIElementTypeButton[@name='Close']");

	private By go = By.id("Go");
	
	public By book_now_final_booking = By.xpath("//XCUIElementTypeButton[@name='BOOK NOW']");
	
	public By back_on_final_booking = By.xpath("//XCUIElementTypeStaticText[@name='Guests']");

	public By done_on_booking_confirmed = By.id("Close");
	
	@Override
	public boolean tapOnPeopleDropdown() 
	{
		try 
		{
			MobileElement peopleDropdown = driver.findElement(people_dropdown);
			peopleDropdown.click();
			
			System.out.println("Select People Dropdown has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Select People Dropdown was not found");
			return false;
		}
	}
	@Override
	public boolean tapOnPeopleDropdownList() 
	{
		try 
		{			

			List <MobileElement> peopleDropdownList = driver.findElements(people_dropdown_List);
			peopleDropdownList.get(2).sendKeys("3 people");			
			
			System.out.println("Select People Dropdown has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Select People Dropdown was not found");
			return false;
		}
	}
	
	@Override
	public boolean tapOnMealDropdown() 
	{
		try 
		{
			MobileElement peopleDropdown = driver.findElement(meal_dropdown);
			peopleDropdown.click();
			List <MobileElement> mealDropdown = driver.findElements(meal_dropdown);
			mealDropdown.get(1).click();
			System.out.println("Select Meal Dropdown has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Select Meal Dropdown was not found");
			return false;
		}
	}
	
	@Override
	public boolean selectCalendarDate() 
	{
		try 
		{
			waitDriver.until(ExpectedConditions.elementToBeClickable(select_calendar_date));

			MobileElement calendar_date = driver.findElement(select_calendar_date);
			calendar_date.click();
			System.out.println("date has been selected");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Calender event was not found");
			return false;
		}
	}
	
	@Override
	public boolean tapOnBookButton() 
	{
		try 
		{
			MobileElement book_button = driver.findElement(tap_on_book_button);
			book_button.click();
			System.out.println("Book button has been Clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Book button was not found");
			return false;
		}
	}
	
	@Override
	public boolean selectTimeSlot() 
	{
		try 
		{
			waitDriver.until(ExpectedConditions.elementToBeClickable(select_time_slot));

			MobileElement time_slot = driver.findElement(select_time_slot);
			time_slot.click();
			System.out.println("Time Slot has been Selected");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Time Slot has not been clicked");
			return false;
		}
	}
	
	@Override
	public boolean entertextbox() 
	{
		
			List<MobileElement> text_box= driver.findElements(textbox);
			int text_box_size=text_box.size();
			
			for (int i=0; i<=text_box_size-1; i++){
				
				
				String sValue = text_box.get(i).getText();
				//System.out.println(sValue);
				if (sValue.equalsIgnoreCase("e.g. John, Ingrid etc.")){

					text_box.get(0).sendKeys("first name");	
					System.out.println(text_box.get(0).getAttribute("value"));
					MobileElement go_keyboard = driver.findElement(go);	
					go_keyboard.click();
					
				}
						if(sValue.equalsIgnoreCase("e.g. Smith, Patel etc.")){
						text_box.get(1).click();
						text_box.get(1).sendKeys("Last name");
						System.out.println(text_box.get(1).getAttribute("value"));
						MobileElement go_keyboard = driver.findElement(go);	
						go_keyboard.click();
						}
							if(sValue.equalsIgnoreCase("e.g. john@bookatable.com")){
								text_box.get(2).click();
								text_box.get(2).sendKeys("vinita.patil@capgemini.com");
								System.out.println(text_box.get(2).getAttribute("value"));
								MobileElement go_keyboard = driver.findElement(go);									
								go_keyboard.click();
							}
							if(sValue.equalsIgnoreCase("e.g. 07000 000000")){
								text_box.get(3).click();
								text_box.get(3).sendKeys("0123456789");
								System.out.println(text_box.get(3).getAttribute("value"));
				}
			}
			return false;
				
	}
	
	public boolean getGo() {
		try 
		{
			MobileElement go_keyboard = driver.findElement(go);	
			go_keyboard.click();
			System.out.println("clicked on Go button");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Close button was not found");
			return false;
		}
	}
	
@Override
public boolean tapOn_Close() 
{
	try 
	{
		MobileElement close = driver.findElement(tap_on_close);
		close.click();
		System.out.println("Close button has been clicked");
		return true;
	}
	catch (Exception e) 
	{
		System.out.println("Close button was not found");
		return false;
	}
}


	@Override
	public boolean tapOn_Continue_button() 
	{
		try 
		{
			MobileElement continue_element = driver.findElement(tap_continue);
			continue_element.click();
			System.out.println("Continue button has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Continue button was not found");
			return false;
		}
	}
	@Override
	public boolean tapOn_Booking_Now_Confirm_button() 
	{
		try 
		{
			MobileElement book_now_final_booking_element = driver.findElement(book_now_final_booking);
			book_now_final_booking_element.click();
			System.out.println("Booking Now button has been clicked on confirm booking.");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Booking Now button was not found on confirm booking.");
			return false;
		}
	}
	
	@Override
	public boolean tapOn_done_on_booking_confirmed() 
	{
		try 
		{
			MobileElement done_on_booking_confirmed_element = driver.findElement(done_on_booking_confirmed);
			done_on_booking_confirmed_element.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("DONE button was not found on confirm booking.");
			return false;
		}
	}
}
	/*public boolean calendarControl() 
	{
	  driver.FindElement(By.Id("com.appzonegroup.dejavuandroid.zoneRevamp:id/date_picker_year")).Click();
	  ((IJavaScriptExecutor)driver).ExecuteScript("mobile: scroll", scrollObject);
	                  int ThisYear = Int32.Parse( _driver.FindElement(By.XPath(AuthConstant.DatePickerYearButton)).Text);
	      String RegistrableYear = Convert.ToString(ThisYear - 25);
	      IList<IWebElement> calcButton = _driver.FindElements(By.Id("com.appzonegroup.dejavuandroid.zoneRevamp:id/month_text_view"));
	                  for(int i=0; i<calcButton.Count; i++)
	                  {
	                      if(calcButton[i].Text == RegistrableYear)
	                      {
	                          new AuthUtils(_driver).WaitPageLoadById(RegistrableYear);
	                          calcButton[i].Click();
	                      }
	                  }
	}*/
	                  
	                /*  
	                 
	                 //XCUIElementTypePickerWheel[1] For Date
					//XCUIElementTypePickerWheel[2] for Month
					//XCUIElementTypePickerWheel[3] for Year
	                List<IOSElement> wheels =  (List<IOSElement>) driver.findElements(By.className("XCUIElementTypePickerWheel"));		
	              	wheels.get(0).sendKeys("8");
	              	wheels.get(1).sendKeys("January");
	              	wheels.get(2).setValue("1970");*/
	
	/*public boolean peopleDropDownControl() {
		
	}*/
	/*public boolean mealDropDownControl(){
		
	}*/
