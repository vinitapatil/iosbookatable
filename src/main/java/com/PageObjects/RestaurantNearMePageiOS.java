package com.PageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;

/**
 * Created by Vinita Patil!
 *
 */
public class RestaurantNearMePageiOS extends RestaurantNearMePage
{
	private IOSDriver<MobileElement> driver;
	
	public RestaurantNearMePageiOS(AppiumDriver<MobileElement> driver) {
		this.driver = (IOSDriver<MobileElement>) driver;
			}
	
	//public By search_Texbox = By.xpath("//XCUIElementTypeSearchField[@name='Restaurant or Location']");
	public By search_Texbox = By.xpath("//XCUIElementTypeOther[1]//XCUIElementTypeSearchField[1]");
	
	public By we_cannot_find_restaurant_text = By.xpath("//XCUIElementTypeStaticText[@name='restaurants-not-found.title-label']");
	
	public By do_not_worry_text = By.xpath("//XCUIElementTypeStaticText[@name='restaurants-not-found.subtitle-label']");
	
	public By search_tab_text = By.id("Search");
	
	public By bookings_tab_text = By.id("Bookings");

	public By select_First_Record = By.xpath("(//XCUIElementTypeStaticText[@name='location-title'])[1]");
	
	public By select_First_Record_For_restaurant_search = By.xpath("//XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]");
	
	public By try_again_button = By.id("restaurants-not-found.retry-button");

	public By frame_Layout = By.className("XCUIElementTypeOther");

	public By image_view = By.className("android.widget.ImageView");
	
	public By name_dropdown_view = By.xpath("//XCUIElementTypeStaticText[@name='location-title']"); 
	  
	public By list_frame_Layout = By.className("XCUIElementTypeOther");
	
	public By restaurant_near_me_icon = By.id("location-title");
	
	public By image_container = By.name("restaurant-listings-screen.restaurant-image-container");

	public By restaurant_name = By.name("restaurant-listings-screen.restaurant-name");

	public By restaurant_address = By.name("restaurant-listings-screen.restaurant-address");
	
	public By try_Again_button = By.name("Try Again");
	
	public By clear_text_button = By.id("Clear text");

	public By Cancel_button = By.xpath("//XCUIElementTypeOther[1]//XCUIElementTypeButton[2]");
	
	public By restaurant_name_listView = By.xpath("(//XCUIElementTypeStaticText[@name='restaurant-listings-screen.restaurant-name'])[1]");

	public By restaurant_address_listView = By.xpath("(//XCUIElementTypeStaticText[@name='restaurant-listings-screen.restaurant-address'])[1]");

	
	public WebElement getImageAvailable() 
	{
		
		MobileElement searchTextBox = driver.findElement(image_container);
			return searchTextBox;
			
	}
	public WebElement getTextforRestaurantName() 
	{
		
		MobileElement searchTextBox = driver.findElement(restaurant_name);
			return searchTextBox;
			
	}
	
	public WebElement getTextforRestaurantAddress() 
	{
		
		MobileElement searchTextBox = driver.findElement(restaurant_address);
			return searchTextBox;
			
	}
	@Override
	  public String getrestaurantNameListView() {
			
			try {
				MobileElement getrestaurantNameListViewElement = driver.findElement(restaurant_name_listView);
				
				return getrestaurantNameListViewElement.getText();
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Name Label is not found");
			}
			
			return null;
		}
	  
	  @Override
	  public String getrestaurantAddressListView() {
			
			try {
				MobileElement dialledNumberLabelelement = driver.findElement(restaurant_address_listView);
				
				return dialledNumberLabelelement.getText();
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Name Label is not found");
			}
			
			return null;
		}
	
	public WebElement searchTextBox() 
		{
			
		MobileElement searchTextBox = driver.findElement(search_Texbox);
				return searchTextBox;
				
		}
	  
	  public boolean tryAgainButton() 
		{
			try 
			{
				MobileElement frameContainer = driver.findElement(try_again_button);
				frameContainer.click();
				System.out.println("Try Again Button has been clicked");
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Try Again button was not found");
				return false;
			}
		}
	  public boolean selectFirstRecordForLocationSearch() 
		{
			try 
			{
				MobileElement selectFirstRecord = driver.findElement(select_First_Record);
				selectFirstRecord.click();
				System.out.println("First location from search result has been selected");
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Location was not found in search result to select.");
				return false;
			}
		}
	  
	  
	  //not required
	  public boolean selectFirstRecordFromtheSearchList() 
		{
			try 
			{
				MobileElement first_location_record = driver.findElement(select_First_Record);
				first_location_record.click();
				System.out.println("first location has been clicked.");
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("First Location was not found in search.");
				return false;
			}
		}
	public boolean imageViewContainer() 
	{
		try 
		{
			MobileElement imageViewContainer = driver.findElement(image_view);
			imageViewContainer.click();
			System.out.println("Add New Button has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Add New button was not found");
			return false;
		}
	}
	@Override
	public boolean selectFirstRestaurantFromTheList() 
	{
		try 
		{
			MobileElement frameContainer = driver.findElement(list_frame_Layout);
			frameContainer.click();
			System.out.println("First restaurent from the location search has been clicked.");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("First Location was not found in search.");
			return false;
		}
	}
	
	@Override
	public boolean tapOnRestaurantNearMe() 
	{
		try 
		{
			MobileElement restaurantNearMeIcon = driver.findElement(restaurant_near_me_icon);
			restaurantNearMeIcon.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Restaurant Near me button was not found.");
			return false;
		}
	}
	@Override
	public String getTextRestaurantNearMe() 
	{
		try 
		{
			MobileElement restaurantNearMeElement = driver.findElement(search_Texbox);
			String restaurantNearMe_actual= restaurantNearMeElement.getText();
			return restaurantNearMe_actual;
		}
		catch (Exception e) 
		{
			System.out.println("Restaurant Near me text was not found.");
			return null;
		}
	}
	@Override
	public String getTextWeCannotFindRestaurantText() 
	{
		try 
		{
			MobileElement we_cannot_find_restaurant_textElement = driver.findElement(we_cannot_find_restaurant_text);
			String we_cannot_find_restaurant_text_actual= we_cannot_find_restaurant_textElement.getText();
			return we_cannot_find_restaurant_text_actual;
		}
		catch (Exception e) 
		{
			System.out.println("We Cannot Find Restaurant text was not found.");
			return null;
		}
	}
	@Override
	public String getTextForDoNotWorry() 
	{
		try 
		{
			MobileElement do_not_worry_textElement = driver.findElement(do_not_worry_text);
			String do_not_worry_text_actual= do_not_worry_textElement.getText();
			return do_not_worry_text_actual;
		}
		catch (Exception e) 
		{
			System.out.println("Do Not Worry text was not found.");
			return null;
		}
	}
	@Override
	public String getTextTryAgainButton() 
	{
		try 
		{
			MobileElement tryAgainButton = driver.findElement(try_again_button);
			String tryAgain_actual =tryAgainButton.getText();
			
			return tryAgain_actual;
		}
		catch (Exception e) 
		{
			System.out.println("Try Again button was not found.");
			return null;
		}
	}
		
	@Override
	public String getTextForSearchTab() 
	{
		try 
		{
			MobileElement search_tab_text_element = driver.findElement(search_tab_text);
			String search_tab_text_actual =search_tab_text_element.getAttribute("label");
			
			return search_tab_text_actual;
		}
		catch (Exception e) 
		{
			System.out.println("Search Tab Text was not found.");
			return null;
		}
	}
	
	@Override
	public String getTextForBookingsTab() 
	{
		try 
		{
			MobileElement bookings_tab_text_element = driver.findElement(bookings_tab_text);
			String bookings_tab_text_actual =bookings_tab_text_element.getText();
			
			return bookings_tab_text_actual;
		}
		catch (Exception e) 
		{
			System.out.println("Bookings tab text was not found.");
			return null;
		}
	}
	
	@Override
	public String getTextForCancelButton() 
	{
		try 
		{
			MobileElement Cancel_button_element = driver.findElement(Cancel_button);
			String Cancel_button_actual =Cancel_button_element.getText();
			
			return Cancel_button_actual;
		}
		catch (Exception e) 
		{
			System.out.println("Bookings tab text was not found.");
			return null;
		}
	}
	@Override
	public MobileElement searchTextbox() {
					
		MobileElement searchTextBox = driver.findElement(search_Texbox);
			return searchTextBox;
			
	}
	@Override
	public boolean searchTextboxEdit() {
					
		try 
		{
			MobileElement searchTextBox = driver.findElement(search_Texbox);
			searchTextBox.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Search textbox was not found.");
			return false;
		}
	}
	@Override
	public void txtRestaurantName() {
		
		List<MobileElement> dropList = driver.findElements(name_dropdown_view);
		
		for(int i = 0; i < dropList.size(); i++) 
		{           
			System.out.println(dropList.get(i).getText());
        }		
	}

	@Override
	public void HideKeyboard() {
		driver.hideKeyboard();
		
	}
	
	public int getDropdownCount() {
		
		//List<MobileElement> dropList = driver.findElements(By.xpath("//XCUIElementTypeOther[@name='master view']/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView"));
		List<MobileElement> dropList = driver.findElements(By.name("location-title"));
		int dropdowncount=dropList.size();
		return dropdowncount;
			
		}
	
	
	public int getLocationCountDropdownList() {
	
		//List<MobileElement> dropList = driver.findElements(By.xpath("//XCUIElementTypeOther[@name='master view']/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView"));
		List<MobileElement> dropList = driver.findElements(By.name("location-title"));
		System.out.println(dropList.size());
		return dropList.size();
	
		}
		public void getlocationNamefromDropDown(){
		
			try 
			{
				MobileElement text = driver.findElement(By.xpath("//*[contains(text(),'City of London')])"));
			
			String gettext= text.getAttribute("text");
			System.out.println(gettext);
			}
			catch(Exception e) 
			{
				System.out.println("ERROR");
			}
	}

		public boolean tapOnClearTextButton(){
			
			try 
			{
				MobileElement clear_textButton_element = driver.findElement(clear_text_button);
				clear_textButton_element.click();
			}
			catch(Exception e) 
			{
				System.out.println("Clear text button was not found.");
			}
			return false;
	}
		@Override
		public boolean tapOnCancelButton(){
			
			try 
			{
				MobileElement cancel_button_element = driver.findElement(Cancel_button);
				cancel_button_element.click();
			}
			catch(Exception e) 
			{
				System.out.println("Cancel button was not found.");
			}
			return false;
	}
		@Override
		public boolean selectFirstRecordForRestaurantSearch() {
			
			try 
			{
				MobileElement select_First_Record_For_restaurant_search_element = driver.findElement(select_First_Record_For_restaurant_search);
				select_First_Record_For_restaurant_search_element.click();
			}
			catch(Exception e) 
			{
				System.out.println("First Record For restaurant search was not found.");
			}
			return false;
		}
		@Override
		public String getTextLocationServiceErrorHeader() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public String getTextLocationServiceErrorDesc() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public String getTextLocationServiceNavigationLink() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public boolean tapOnLocationServiceNavigationLink() {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public boolean tapOnDeviceLocationService() {
			// TODO Auto-generated method stub
			return false;
		}
		@Override
		public boolean tapOnDeviceBackButton() {
			// TODO Auto-generated method stub
			return false;
		}
		
	

}
