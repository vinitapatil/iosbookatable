package com.PageObjects;

import java.util.List;

import io.appium.java_client.MobileElement;

public abstract class BDAScreen {

	public abstract boolean tapOnPeopleDropdown();

	public abstract boolean selectTimeSlot();

	public abstract boolean selectCalendarDate();

	public abstract boolean tapOn_Close();

	public abstract boolean tapOnMealDropdown();

	public abstract boolean tapOnBookButton();
	
	public abstract boolean  tapOnPeopleDropdownList();

	public abstract boolean entertextbox();

	public abstract boolean tapOn_Continue_button();

	public abstract boolean tapOn_Booking_Now_Confirm_button();

	public abstract boolean tapOn_done_on_booking_confirmed();
}
