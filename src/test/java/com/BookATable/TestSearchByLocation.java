package com.BookATable;

import java.io.IOException;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

//@Listeners(com.BookATable.Listener.class)	
public class TestSearchByLocation extends BaseTestInitializeClass {
		
	@Test(priority = 8)
	public void searchByLocation() throws InterruptedException, IOException {
		// logger = report.startTest("TestSearchByRestaurant");
		// logger.log(Status.INFO, "Test Search By Restaurant");

		System.out.println("Landed on Restaurant Listing Screen");
		logger = report.createTest("Restaurant Near Me: Search By Location",
				"Verify functionality of search by Location.");
		if (restaurantNearMePage.searchTextbox() != null) {
			String searchLocation = "London";
			restaurantNearMePage.searchTextboxEdit();
			restaurantNearMePage.searchTextbox().sendKeys(searchLocation);
			System.out.println("Location Entered. " + searchLocation);
			restaurantNearMePage.HideKeyboard();
			System.out.println(
					"Search result found : " + searchLocation + " is "  +restaurantNearMePage.getDropdownCount());
			
			logger.log(Status.PASS, MarkupHelper.createLabel("Search Location entered : "+ searchLocation , ExtentColor.GREEN));
			
			logger.log(Status.PASS, MarkupHelper.createLabel("Number of Search result for location search: "+"\" "+searchLocation+" \""+ " is " +restaurantNearMePage.getDropdownCount(), ExtentColor.GREEN));
						
		} else {
			System.out.println("Search Text was not entered.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to enter search text.", ExtentColor.RED));		
		}
	}

	@Test(priority = 9)
	public void selectFirstRecordForLocationSearch() throws IOException{
		logger = report.createTest("Restaurant Listing Page: Select First Record For Location Search",
				"Verify functionality of selecting first restaurant from the list.");

		if (restaurantNearMePage.selectFirstRecordForLocationSearch()) {
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Tapped on first record from Location search Result.", ExtentColor.GREEN));
			System.out.println("Tapped on location from Location search Result.");

		} else {
			System.out.println("Failed to find the location from location search result.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to find the location from location search result.",
					ExtentColor.RED));			
		}
	}

	@Test(priority = 10)
	public void verifyRestaurantNameAndAddress() throws IOException {
		logger = report.createTest("Restaurant Listing Page: Verify Restaurant Name And Address Text",
				"Verify Restaurant Name and Address is present.");

		if (restaurantNearMePage.getrestaurantNameListView() != null) {
			System.out.println("Restaurant Name is :" + restaurantNearMePage.getrestaurantNameListView());
			logger.log(Status.PASS, MarkupHelper.createLabel(
					"Restaurant Name is :" + restaurantNearMePage.getrestaurantNameListView(), ExtentColor.GREEN));

		} else {
			System.out.println("Failed to find the Name of restaurant.");
			logger.log(Status.FAIL,
					MarkupHelper.createLabel("Failed to find the Name of restaurant.", ExtentColor.RED));
			
		}
		if (restaurantNearMePage.getrestaurantAddressListView() != null) {
			System.out.println("Restaurant Address is :" + restaurantNearMePage.getrestaurantAddressListView());
			logger.log(Status.PASS,
					MarkupHelper.createLabel(
							"Restaurant Address is :" + restaurantNearMePage.getrestaurantAddressListView(),
							ExtentColor.GREEN));

		} else {
			System.out.println("Failed to find the Address of restaurant.");
			logger.log(Status.FAIL,
					MarkupHelper.createLabel("Failed to find the Address of restaurant.", ExtentColor.RED));			
		}
	}

	@Test(priority = 11)
	public void verifyVerticalScrollInRestaurantList() {
		logger = report.createTest("Restaurant Listing Page :Vertical Scroll",
				"Verify the functionality of Vertical Scroll In Restaurant Listing Page.");
		restaurantDetailsPage.swipeFromBottomToUp();
		
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Diner is able to scroll from bottom to top.", ExtentColor.GREEN));
				
		restaurantDetailsPage.swipeFromBottomToUp();

		restaurantDetailsPage.swipeFromUpToBottom();
		
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Diner is able to scroll from top to bottom.", ExtentColor.GREEN));	
		
		restaurantDetailsPage.swipeFromUpToBottom();
	}

	@Test(priority = 12)
	public void selectFirstRestaurantFromTheList() {
		logger = report.createTest("Restaurant Listing Page :selectFirstRestaurantFromTheList",
				"Verify functionality of selecting first restaurant from the list.");

		if (restaurantNearMePage.selectFirstRestaurantFromTheList()) {
			System.out.println("Tapped on Restaurant from Location search Result.");
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Tapped on Restaurant from Location search Result.", ExtentColor.GREEN));
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		} else {
			System.out.println("Restaurant was not found to select.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to find the restaurant.", ExtentColor.RED));
			
		}
	}

	/*
	 * @Test(priority=14) public void verifyVerticalScrollInDetailsPage(){
	 * if(restaurantDetailsPage.swipeFromBottomToUp()) {
	 * System.out.println("Scroll down was successful.");
	 * logger.log(Status.PASS, "Scroll down was successful."); } else {
	 * System.out.println("Failed to scroll down."); logger.log(Status.FAIL,
	 * "Failed to scroll down."); utils.takeScreenshot("Search", "SearchTest");
	 * } if(restaurantDetailsPage.swipeFromUpToBottom()) {
	 * System.out.println("Scroll up was successful."); logger.log(Status.PASS,
	 * "Scroll up was successful."); } else {
	 * System.out.println("Failed to scroll up."); logger.log(Status.FAIL,
	 * "Failed to scroll up."); utils.takeScreenshot("Search", "SearchTest"); }
	 * }
	 */

	@Test(priority = 28)
	public void tapOnBookNowButton() throws InterruptedException {
		logger = report.createTest("Restaurant Details Page: Tap On Book Now Button",
				"Verify functionality of tap on Book Now button from Restaurant Details Page.");

		if (restaurantDetailsPage.tapOnBookNowButtonfunction()) {

			System.out.println("Clicked on Book Now Button.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Book Now Button.", ExtentColor.GREEN));
		} else {
			System.out.println("Book Now Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on Book Now.", ExtentColor.RED));
		
		}
	}

	@Test(priority = 29)
	public void selectCalendarDateforBooking() throws InterruptedException {
		logger = report.createTest("BDA Screen: Select Calendar Date",
				"Verify functionality of selection of date from calendar on BDA Screen.");

		if (bdaScreen.selectCalendarDate()) {
			System.out.println("Diner has selected date from calendar.");
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Diner has selected date from calendar.", ExtentColor.GREEN));

		} else {
			System.out.println("Failed to selectd date from the calendar.");
			logger.log(Status.FAIL,
					MarkupHelper.createLabel("Failed to selectd date from the calendar.", ExtentColor.RED));
			
		}
	}

	@Test(priority = 30)
	public void tapOnBookButtonOnBDAScreen() throws InterruptedException {
		logger = report.createTest("BDA Screen: Tap On Book Button On BDA Screen",
				"Verify functionality of Book button on BDA Screen.");

		if (bdaScreen.tapOnBookButton()) {

			System.out.println("Clicked on Book Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Book Now Button.", ExtentColor.GREEN));

		} else {
			System.out.println("Book Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on book button.", ExtentColor.RED));
			
		}
	}

	@Test(priority = 31)
	public void selectTimeSlotOnBDAScreen() throws InterruptedException {
		logger = report.createTest("BDA Screen: select Time Slot On BDA Screen",
				"Verify functionality of time slot selection by the diner.");

		if (bdaScreen.selectTimeSlot()) {

			System.out.println("Time slot has been selected.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Time slot has been selected.", ExtentColor.GREEN));

		} else {
			System.out.println("Time slot Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to select the time slot.", ExtentColor.RED));
			
		}
		restaurantDetailsPage.swipeFromBottomToUp();

	}

	@Test(priority = 32)
	public void enterDinerContactDetailsOnBDAScreen() throws InterruptedException {
		logger = report.createTest("BDA Screen: Enter Diner Details On BDA Screen",
				"Verify Diner is able to enter contact details");

		if (bdaScreen.entertextbox()) {
			System.out.println("Diner Contact Details has been Entered");
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Diner Contact Details has been Entered", ExtentColor.GREEN));

		} else {
			System.out.println("Failed to enter Diner Contact Details.");
			logger.log(Status.FAIL,
					MarkupHelper.createLabel("Failed to enter Diner Contact Details.", ExtentColor.RED));
		
		}
	}

	/*
	 * if(bdaScreen.tapOn_Close()) {
	 * 
	 * System.out.println("Clicked on Close Button"); logger.log(Status.PASS,
	 * "Clicked on Close Button");
	 * 
	 * } else { System.out.println("Close Button could not be clicked.");
	 * logger.log(Status.FAIL, "Failed to tap on Close button.");
	 * 
	 * utils.takeScreenshot("Search", "SearchTest"); }
	 */
	@Test(priority = 33)
	public void tapOnContinueButtonOnBDAScreen() throws InterruptedException, IOException {
		logger = report.createTest("BDA Screen: Tap On Continue Button On BDA Screen",
				"Verify functionality of Continue Button On BDA Screen.");

		if (bdaScreen.tapOn_Continue_button()) {

			System.out.println("Clicked on continue Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on continue Button", ExtentColor.GREEN));

		} else {
			System.out.println("Failed to tap on continue button.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on continue button.", ExtentColor.RED));	
		}
		restaurantDetailsPage.swipeFromBottomToUp();
	}
		@Test(priority = 34)
		public void tapOnBookNowConfirmBookingButton() throws InterruptedException, IOException {
			logger = report.createTest("BDA Screen: Tap On Book Now Confirm Booking.",
					"Verify functionality of Book Now Button On BDA Screen.");

		
		if (bdaScreen.tapOn_Booking_Now_Confirm_button()) {

			System.out.println("Clicked on Booking Now Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Booking Now Button", ExtentColor.GREEN));

		} else {
			System.out.println("Failed to tap on Booking Now button on confirm booking.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on Booking Now button on confirm booking.",
					ExtentColor.RED));			
		}
	}

	@Test(priority = 35)
	public void tapOnDONEButtonOnBDAScreen() throws InterruptedException, IOException {
		Thread.sleep(1000);
		logger = report.createTest("BDA Screen: Tap On DONE Button On BDA Screen",
				"Verify functionality of Done button on confirm booking on BDA Screen.");

		if (bdaScreen.tapOn_done_on_booking_confirmed()) {
			System.out.println("Clicked on DONE button on Booking Confirm Screen.");
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Clicked on DONE button on Booking Confirm Screen.", ExtentColor.GREEN));

		} else {
			System.out.println("Failed to tap on DONE button on booking confirm  screen.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on DONE button on booking confirm  screen.",
					ExtentColor.RED));			
		}
	}
	public void tapOnBackButtonOnBDAScreen() throws InterruptedException, IOException {
		Thread.sleep(1000);
		logger = report.createTest("BDA Screen: tap On Back Button On BDA Screen",
				"Verify functionality of Back button on BDA Screen.");
		if (restaurantDetailsPage.tapOnBackButton()) {

			System.out.println("Clicked on Back Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Back Button", ExtentColor.GREEN));

		} else {
			System.out.println("Back Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on back button.", ExtentColor.RED));
	
		}

		// report.endTest(logger);
		// report.flush();
	}

}
