package com.BookATable;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
public class TestToVerifyContacts extends BaseTestInitializeClass{
	
	@Test(priority=20)
	public void tapOnPhoneNumber() throws InterruptedException{
		//logger = report.startTest("TestToVerifyContacts");
		//logger.log(Status.INFO, "Test To Verify Images");
		logger= report.createTest("Restaurant Details Page: tapOnContactTab", "Verify Diner is able to tap on Contact Tab..");
	
		if(restaurantDetailsPage.tapOnContactTab())
			{
			logger.log(Status.PASS, MarkupHelper.createLabel("Tapped on Contact Tab from Restaurant detail Page" , ExtentColor.GREEN));

				System.out.println("Tapped on Contact Tab from Restaurant detail Page");	
			}
			else
			{
				System.out.println("Failed to tap on Contact Tab from Restaurant details page.");
				logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on Contact Tab from Restaurant details page.", ExtentColor.RED));
				//utils.takeScreenshot("Search", "SearchTest");
			}
		
		logger= report.createTest("Restaurant Details Page: tapOnPhoneNumber", "Verify Diner is able to tap on Phone number.");

		if(restaurantDetailsPage.tapOnPhoneNumber() != null)
		{
			System.out.println(restaurantDetailsPage.getContactText());		

			restaurantDetailsPage.tapOnPhoneNumber().click();
			logger.log(Status.PASS, MarkupHelper.createLabel("Tapped on Phone number from the Contact Tab of Restaurent Detail Page." , ExtentColor.GREEN));

			System.out.println("Tapped on Phone number from the Contact Tab of Restaurent Detail Page.");
		}
		else
		{
			logger.log(Status.FAIL, MarkupHelper.createLabel("Phone number could not be clicked.", ExtentColor.RED));

			System.out.println("Phone number could not be clicked.");
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority=21)
	public void tapOnCancelCallButton() throws InterruptedException{
		logger= report.createTest("Restaurant Details Page: tapOnCancelCallButton", "Verify Diner is able to tap on Cancel Call pop up.");

		if(restaurantDetailsPage.tapOnCancelCall())
		{
			
			System.out.println("Cancel Call button has been clicked.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Cancel Call button has been clicked." , ExtentColor.GREEN));

		}
		else
		{
			logger.log(Status.FAIL, MarkupHelper.createLabel("Cancel button could not be clicked.", ExtentColor.RED));
			System.out.println("Cancel button could not be clicked.");
			//utils.takeScreenshot("Search", "SearchTest");
		}
		//report.endTest(logger);
		//report.flush();
	}
	
}
