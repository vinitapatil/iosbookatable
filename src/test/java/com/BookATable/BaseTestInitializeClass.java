package com.BookATable;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.mail.EmailException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.MobileBaseClasses.MobileBaseClass;
import com.MobileBaseClasses.MobileDeviceController;
import com.PageObjects.*;
import com.ScreenShot.utils.Utils;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.monitoringMail.utility.SendMail;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BaseTestInitializeClass extends MobileBaseClass {

	protected SplashScreen splashScreenPage;

	protected RestaurantNearMePage restaurantNearMePage;

	protected RestaurantDetailsPage restaurantDetailsPage;

	protected BDAScreen bdaScreen;

	protected MyBookingPage myBookingsPage;

	protected AppiumDriver<MobileElement> driver;
	
	protected ExtentTest logger;

	protected ExtentHtmlReporter htmlReporter;

	protected ExtentReports report;

	Utils utils = new Utils();
	Date date = new Date();
	SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");
	String simpledate = df.format(date);
	// ExtentReports report = new
	// ExtentReports("./extentreport/SearchReport_"+simpledate+".html", false);
	/*@BeforeSuite
	public void reportSetup(){
		htmlReporter = new ExtentHtmlReporter("./extentreport/AutomationReport.html");
		report = new ExtentReports();
		report.attachReporter(htmlReporter);

		report.setSystemInfo("App Name", "Bookatable");

		htmlReporter.config().setChartVisibilityOnOpen(true);
		htmlReporter.config().setDocumentTitle("Automation Report");
		htmlReporter.config().setReportName("Bookatable Automation Test Report");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);
	}*/
	@BeforeClass
	public void setUp() throws Exception {
		// logger = report.startTest("TestSearchByRestaurant", "");
		htmlReporter = new ExtentHtmlReporter("./extentreport/AutomationReport.html");
		report = new ExtentReports();
		report.attachReporter(htmlReporter);

		report.setSystemInfo("App Name", "Bookatable");

		htmlReporter.config().setChartVisibilityOnOpen(true);
		htmlReporter.config().setDocumentTitle("Automation Report");
		htmlReporter.config().setReportName("Bookatable Automation Test Report");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);

		MobileDeviceController.instance.start();

		switch (MobileDeviceController.executionOS) {

		case ANDROID:

			splashScreenPage = new SplashScreenAndroid(driver());

			restaurantNearMePage = new RestaurantNearMePageAndroid(driver());

			restaurantDetailsPage = new RestaurantDetailsPageAndroid(driver());

			bdaScreen = new BDAScreenAndroid(driver());

			break;

		case IOS:

			splashScreenPage = new SplashScreeniOS(driver());

			restaurantNearMePage = new RestaurantNearMePageiOS(driver());

			restaurantDetailsPage = new RestaurantDetailsPageiOS(driver());

			bdaScreen = new BDAScreeniOS(driver());

			myBookingsPage = new MyBookingPageiOS(driver());

			break;

		}
	}

	@AfterMethod
	public void getResult(ITestResult result) throws IOException {	

		if (result.getStatus() == ITestResult.FAILURE) {
			
			logger.log(Status.FAIL, "Test Case Failed is " + result.getName());
			logger.log(Status.FAIL, "Test Case Failed is " + result.getThrowable());
			logger.fail(utils.takeScreenshot(driver, result.getName()));
		
			String screenShotPath = utils.takeScreenshot(driver, "Bookatable");
					
			logger.addScreenCaptureFromPath(screenShotPath);
			
		} 
		else {
			

			//logger.log(Status.SKIP,"Test Case Skipped : " +result.getName());
		}
		//report.removeTest(logger);
		report.flush();
	}
	
	//  @AfterTest public void tearDown() { report.flush(); }
	 

	@AfterSuite
	public void sendMail() throws EmailException {
		
		//report.flush();
		SendMail.sendEmail();
		System.out.println("Automation Report sent.");
	}
}