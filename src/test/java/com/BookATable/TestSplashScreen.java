package com.BookATable;


import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import com.BookATable.BaseTestInitializeClass;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
//@Listeners(com.BookATable.Listener.class)	

public class TestSplashScreen extends BaseTestInitializeClass{	
	
	@Test(priority=1)
//1. Launch>1.1- New User/First Time User>SplashScreen Launch>SplashScreen Loader>Location Service Permission pop up>ALLOW > Land on Restaurant Near Me
	
	public void appLaunchALLOWPermissionPopUp() throws InterruptedException{
		//logger.log(Status.INFO, "Diner Landed on Splash Screen");
	
		/*if(splashScreenPage.getPermissionMessage() != null)
		{
			System.out.println(splashScreenPage.getPermissionMessage());
			Assert.assertEquals(splashScreenPage.getPermissionMessage(), "Allow “Bookatable” to access your location while you use the app?");
			logger.log(Status.PASS, MarkupHelper.createLabel("Location permission message matched.", ExtentColor.GREEN));
			System.out.println("Location permission message matched.");
		}
		else
		{
			logger.log(Status.FAIL, MarkupHelper.createLabel("Location permission message does not match.", ExtentColor.RED));
			System.out.println("Location permission message does not match.");
			//utils.takeScreenshot("Search", "SearchTest");

		}*/
			logger = report.createTest("Splash Screen: permissionAllowButton", "Verify functionality of ALLOW on permission pop up.");
			
			if (splashScreenPage.permissionAllowButton()) {
				logger.log(Status.PASS, MarkupHelper.createLabel("ALLOW Button clicked", ExtentColor.GREEN));
				System.out.println("ALLOW Button clicked");
			} else {
				logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to click on ALLOW on permission pop up.", ExtentColor.RED));
				System.out.println("ALLOW Button could not be clicked");
				// utils.takeScreenshot("Search", "SearchTest");

			}
			Thread.sleep(2000);
		
		//report.endTest(logger);
		//report.flush();
	}
	
	
}