package com.BookATable;

import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class TestFunctionalityOfSearchBox extends BaseTestInitializeClass {
	private AndroidDriver<MobileElement> driver;

	@Test(priority = 2)

	public void searchByOneCharacter() throws InterruptedException {
		// logger = report.startTest("TestFunctionalityOfSearchBox");
		// logger.log(Status.INFO, "Diner Test Functionality Of SearchBox");
		 this.driver = (AndroidDriver<MobileElement>) driver;

		logger = report.createTest("searchByOneCharacter", "Verify Functionality Of SearchBox for 1 Character search.");
		if (restaurantNearMePage.searchTextbox() != null) {
			String search = "L";
			restaurantNearMePage.searchTextboxEdit();
			restaurantNearMePage.searchTextbox().sendKeys(search);

			System.out.println("Location Entered. " + search);

			restaurantNearMePage.HideKeyboard();
			System.out.println("Search result found for 1 character search.: " + search + " is "
					+ +restaurantNearMePage.getDropdownCount());

			restaurantNearMePage.txtRestaurantName();
			
			restaurantNearMePage.searchTextbox().clear();
			logger.log(Status.PASS, MarkupHelper.createLabel("Search result found for 1 character search.: " + search
					+ " is " + +restaurantNearMePage.getDropdownCount(), ExtentColor.GREEN));
		} else {
			System.out.println("Search Textbox was not found");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Search Textbox was not found", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 3)
	public void searchByTwoCharacter() throws InterruptedException {
		logger = report.createTest("searchByTwoCharacter", "Verify Functionality Of SearchBox for 2 Character search.");

		if (restaurantNearMePage.searchTextbox() != null) {
			String search = "Ge";
			restaurantNearMePage.searchTextbox().sendKeys("Ge");
			System.out.println("Location Entered. " + search);

			restaurantNearMePage.HideKeyboard();
			System.out.println("Search result found for 2 character search.: " + search + " is "
					+ +restaurantNearMePage.getDropdownCount());

			restaurantNearMePage.txtRestaurantName();

			restaurantNearMePage.searchTextbox().clear();
			logger.log(Status.PASS, MarkupHelper
					.createLabel("Location search count and list retrived for 2 character search", ExtentColor.GREEN));

		} else {
			System.out.println("Search Textbox was not found");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Search Textbox was not found", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 4)
	public void searchByThreeCharacter() throws InterruptedException {
		logger = report.createTest("searchByThreeCharacter",
				"Verify Functionality Of SearchBox for 3 Character search.");

		if (restaurantNearMePage.searchTextbox() != null) {
			String search = "Lee";
			restaurantNearMePage.searchTextbox().sendKeys(search);
			System.out.println("Location Entered. " + search);

			restaurantNearMePage.HideKeyboard();

			System.out.println("Search result found for 3 character search.: " + search + " is "
					+ +restaurantNearMePage.getDropdownCount());

			restaurantNearMePage.txtRestaurantName();

			restaurantNearMePage.searchTextbox().clear();
			logger.log(Status.PASS, MarkupHelper
					.createLabel("Location search count and list retrived for 3 character search", ExtentColor.GREEN));

		} else {
			System.out.println("Search Textbox was not found");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Search Textbox was not found", ExtentColor.RED));

			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 5)
	public void invalidSearch() throws InterruptedException {

		logger = report.createTest("invalidSearch", "Verify Invalid search");

		if (restaurantNearMePage.searchTextbox() != null) {
			String search = "AAA_987";

			restaurantNearMePage.searchTextbox().sendKeys(search);
			restaurantNearMePage.tapOnCancelButton();
			// restaurantNearMePage.HideKeyboard();

			System.out.println("Search result found for invalid search.: " + search + " is "
					+ +restaurantNearMePage.getDropdownCount());

			restaurantNearMePage.searchTextbox().clear();
			logger.log(Status.PASS, MarkupHelper.createLabel(
					"Location search count and list retrived for invalid character search", ExtentColor.GREEN));

		} else {
			System.out.println("Go Button could not be clicked");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Search Textbox was not found", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	/*
	 * @Test(priority=6) public void verifyCancelButtonOnSearch() throws
	 * InterruptedException{
	 * 
	 * String search= "Test Cancel Button";
	 * 
	 * restaurantNearMePage.searchTextbox().sendKeys(search);
	 * 
	 * if(restaurantNearMePage.tapOnCancelButton()) {
	 * System.out.println("Cancel button has been clicked in the search box.");
	 * logger.log(Status.PASS, "Cancel button has been clicked in the search box.");
	 * 
	 * } else { //System.out.println("Cancel button was not found.");
	 * logger.log(Status.FAIL, "Failed to tap on cancel button in search box.");
	 * utils.takeScreenshot("Search", "SearchTest"); }
	 * 
	 * }
	 */

	/*
	 * @Test(priority=7) public void verifyClearTextbuttonOnSearch() throws
	 * InterruptedException{
	 * 
	 * String search= "Test Clear Button";
	 * 
	 * restaurantNearMePage.searchTextbox().sendKeys(search);
	 * restaurantNearMePage.HideKeyboard();
	 * 
	 * if(restaurantNearMePage.tapOnClearTextButton()) {
	 * System.out.println("Clear text button has been clicked.");
	 * logger.log(Status.PASS, "Clear text button button has been clicked.");
	 * 
	 * } else { System.out.println("Clear text button was not found.");
	 * logger.log(Status.FAIL, "Clear text button was not found.");
	 * utils.takeScreenshot("Search", "SearchTest"); } }
	 */
	@Test(priority = 7)
	public void restaurentsNearMe() throws InterruptedException {

		restaurantNearMePage.searchTextbox().click();
		logger = report.createTest("restaurentsNearMe", "Verify Restaurents Near Me from search.");

		if (restaurantNearMePage.tapOnRestaurantNearMe()) {
			System.out.println("Restaurent near me button has been clicked by the Diner.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Restaurent near me button has been clicked by the Diner.",
					ExtentColor.GREEN));

		} else {
			System.out.println("Restaurent near me button was not found.");
			//utils.takeScreenshot("Search", "SearchTest");
			logger.log(Status.FAIL,
					MarkupHelper.createLabel("Failed to click on Restaurent near me button.", ExtentColor.RED));

		}
		// report.endTest(logger);
		// report.flush();
	}

}
