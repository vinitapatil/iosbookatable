package com.BookATable;

import java.io.IOException;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class TestGermanLanguage extends LocalizationConfigReader {

	@Test
	public void verifyGermanLanguage() throws IOException{	
		logger = report.createTest("BookATable verifyGermanLanguage",
				"Verify German Lable text");
		
		splashScreenPage.permissionAllowButton();		
		
		restaurantNearMePage.searchTextboxEdit();
		
		restaurantNearMePage.tapOnRestaurantNearMe();			
		
		String location_error_share_your_location_title_text_expected = getObject("location_error.share_your_location_title_text");
		
		if(restaurantNearMePage.getTextLocationServiceErrorHeader().equals(location_error_share_your_location_title_text_expected)){
			logger.log(Status.PASS, MarkupHelper.createLabel("Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +location_error_share_your_location_title_text_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +location_error_share_your_location_title_text_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "Restaurant Near Me Page text does not match", ExtentColor.RED));
		}
		
		String location_error_share_your_location_description_text_expected = getObject("location_error.share_your_location_description_text");
		
		if(restaurantNearMePage.getTextLocationServiceErrorDesc().equals(location_error_share_your_location_description_text_expected)){
			logger.log(Status.PASS, MarkupHelper.createLabel("Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +location_error_share_your_location_description_text_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +location_error_share_your_location_description_text_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "Restaurant Near Me Page text does not match", ExtentColor.RED));
		}
		
		String location_error_turn_on_location_services_button_expected = getObject("location_error.turn_on_location_services_button");
		
		if(restaurantNearMePage.getTextLocationServiceNavigationLink().equals(location_error_turn_on_location_services_button_expected)){
			logger.log(Status.PASS, MarkupHelper.createLabel("Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +location_error_turn_on_location_services_button_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +location_error_turn_on_location_services_button_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "Restaurant Near Me Page text does not match", ExtentColor.RED));
		}
		restaurantNearMePage.tapOnLocationServiceNavigationLink(); //android
		
		restaurantNearMePage.tapOnDeviceLocationService(); //android
		
		restaurantNearMePage.tapOnDeviceBackButton(); //android
		
		String recent_history_restaurants_near_me_expected = getObject("recent_history.restaurants_near_me");
		
		if(restaurantNearMePage.getTextRestaurantNearMe().equals(recent_history_restaurants_near_me_expected)){
			logger.log(Status.PASS, MarkupHelper.createLabel("Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +recent_history_restaurants_near_me_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +recent_history_restaurants_near_me_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "Restaurant Near Me Page text does not match", ExtentColor.RED));
		}
		
		String restaurants_not_found_title_text_expected = getObject("restaurants_not_found.title_text");
		
		if(restaurantNearMePage.getTextWeCannotFindRestaurantText().equals(restaurants_not_found_title_text_expected)){
			logger.log(Status.PASS, MarkupHelper.createLabel("Actual :"+restaurantNearMePage.getTextWeCannotFindRestaurantText()+ " Expected :" +restaurants_not_found_title_text_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextWeCannotFindRestaurantText()+ " Expected :" +restaurants_not_found_title_text_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL, MarkupHelper.createLabel("text does not match for : " + restaurantNearMePage.getTextWeCannotFindRestaurantText(), ExtentColor.RED));
		}
		
		String restaurants_not_found_subtitle_text_expected = getObject("restaurants_not_found.subtitle_text");

		if(restaurantNearMePage.getTextForDoNotWorry().equals(restaurants_not_found_subtitle_text_expected)){
			logger.log(Status.PASS, MarkupHelper.createLabel("Actual :"+restaurantNearMePage.getTextForDoNotWorry()+ " Expected :" +restaurants_not_found_subtitle_text_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextForDoNotWorry()+ " Expected :" +restaurants_not_found_subtitle_text_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "text does not match for : " +restaurantNearMePage.getTextForDoNotWorry(), ExtentColor.RED));
		}
							
		String error_retry_button_text_expected = getObject("error.retry_button_text");
				
		if(restaurantNearMePage.getTextTryAgainButton().equals(error_retry_button_text_expected)){
			logger.log(Status.PASS,MarkupHelper.createLabel( "Actual :"+restaurantNearMePage.getTextTryAgainButton()+ " Expected :" +error_retry_button_text_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextTryAgainButton()+ " Expected :" +error_retry_button_text_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL, MarkupHelper.createLabel("text does not match for : " +restaurantNearMePage.getTextTryAgainButton(), ExtentColor.RED));
		}
		
		String tab_bar_more_expected = getObject("tab_bar.more");
		
		if(restaurantNearMePage.getTextForSearchTab().equals(tab_bar_more_expected)){
			logger.log(Status.PASS, MarkupHelper.createLabel("Actual :"+restaurantNearMePage.getTextForSearchTab()+ " Expected :" +tab_bar_more_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextForSearchTab()+ " Expected :" +tab_bar_more_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL, MarkupHelper.createLabel("text does not match for : " +restaurantNearMePage.getTextForSearchTab(), ExtentColor.RED));
		}	
			
		System.out.println(restaurantNearMePage.getTextForBookingsTab());
		//Need to update property file
		
		restaurantNearMePage.searchTextbox().click();
		String controls_search_bar_placeholder_text_expected = getObject("controls.search_bar_placeholder_text");

		if(restaurantNearMePage.getTextRestaurantNearMe().equals(controls_search_bar_placeholder_text_expected)){
			logger.log(Status.PASS,MarkupHelper.createLabel( "Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +controls_search_bar_placeholder_text_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextRestaurantNearMe()+ " Expected :" +controls_search_bar_placeholder_text_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL, MarkupHelper.createLabel("text does not match for " +restaurantNearMePage.searchTextbox(), ExtentColor.RED));
		}
		
		String common_alert_button_cancel_expected = getObject("common_alert_button.cancel");

		if(restaurantNearMePage.getTextForCancelButton().equals(common_alert_button_cancel_expected)){
			logger.log(Status.PASS, MarkupHelper.createLabel("Actual :"+restaurantNearMePage.getTextForCancelButton()+ " Expected :" +common_alert_button_cancel_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantNearMePage.getTextForCancelButton()+ " Expected :" +common_alert_button_cancel_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL, MarkupHelper.createLabel("text does not match for " +restaurantNearMePage.getTextForCancelButton(), ExtentColor.RED));
		}
		
		restaurantNearMePage.searchTextbox().sendKeys("Cafe Ansari");
		
		restaurantNearMePage.selectFirstRecordForRestaurantSearch();
		
		String restaurant_details_info_button_expected = getObject("restaurant_details.info_button");

		if(restaurantDetailsPage.getTextForInfoTab().equals(restaurant_details_info_button_expected)){
			logger.log(Status.PASS,MarkupHelper.createLabel( "Actual :"+restaurantDetailsPage.getTextForInfoTab()+ " Expected :" +restaurant_details_info_button_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantDetailsPage.getTextForInfoTab()+ " Expected :" +restaurant_details_info_button_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL, MarkupHelper.createLabel("text does not match for " +restaurantDetailsPage.getTextForInfoTab(), ExtentColor.RED));
		}
		
		String restaurant_details_contact_button_expected = getObject("restaurant_details.contact_button");

		if(restaurantDetailsPage.getTextForContactTab().equals(restaurant_details_contact_button_expected)){
			logger.log(Status.PASS,MarkupHelper.createLabel( "Actual :"+restaurantDetailsPage.getTextForContactTab()+ " Expected :" +restaurant_details_contact_button_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantDetailsPage.getTextForContactTab()+ " Expected :" +restaurant_details_contact_button_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "text does not match for " +restaurantDetailsPage.getTextForContactTab(), ExtentColor.RED));
		}
		
		String restaurant_details_opening_hours_title_expected = getObject("restaurant_details.opening_hours_title");
		if(restaurantDetailsPage.getRestaurantDetailsOpeningHoursTitleLabel().equals(restaurant_details_opening_hours_title_expected)){
			logger.log(Status.PASS,MarkupHelper.createLabel( "Actual :"+restaurantDetailsPage.getRestaurantDetailsOpeningHoursTitleLabel()+ " Expected :" +restaurant_details_opening_hours_title_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantDetailsPage.getRestaurantDetailsOpeningHoursTitleLabel()+ " Expected :" +restaurant_details_opening_hours_title_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "text does not match for " +restaurantDetailsPage.getRestaurantDetailsOpeningHoursTitleLabel(), ExtentColor.RED));
		}
		
		String restaurant_details_description_title_expected = getObject("restaurant_details.description_title");

		if(restaurantDetailsPage.getRestaurantDetailsDescriptionTitleLabel().equals(restaurant_details_description_title_expected)){
			logger.log(Status.PASS,MarkupHelper.createLabel( "Actual :"+restaurantDetailsPage.getRestaurantDetailsDescriptionTitleLabel()+ " Expected :" +restaurant_details_description_title_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantDetailsPage.getRestaurantDetailsDescriptionTitleLabel()+ " Expected :" +restaurant_details_description_title_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "text does not match for " +restaurantDetailsPage.getRestaurantDetailsDescriptionTitleLabel(), ExtentColor.RED));
		}
		
		restaurantDetailsPage.tapOnContactTab();
		
		String restaurant_details_location_title_expected = getObject("restaurant_details.location_title");
		if(restaurantDetailsPage.getTextForBookNowButton().equals(restaurant_details_location_title_expected)){
			logger.log(Status.PASS, MarkupHelper.createLabel("Actual :"+restaurantDetailsPage.getTextForBookNowButton()+ " Expected :" +restaurant_details_location_title_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantDetailsPage.getTextForBookNowButton()+ " Expected :" +restaurant_details_location_title_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "text does not match for " +restaurantDetailsPage.getTextForBookNowButton(), ExtentColor.RED));
		}
		
		String restaurant_details_phone_title_expected = getObject("restaurant_details.phone_title");

		if(restaurantDetailsPage.getRestaurantDetailsPhoneValueLabel().equals(restaurant_details_phone_title_expected)){
			logger.log(Status.PASS,MarkupHelper.createLabel( "Actual :"+restaurantDetailsPage.getRestaurantDetailsPhoneValueLabel()+ " Expected :" +restaurant_details_phone_title_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantDetailsPage.getRestaurantDetailsPhoneValueLabel()+ " Expected :" +restaurant_details_phone_title_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "text does not match for " +restaurantDetailsPage.getRestaurantDetailsPhoneValueLabel(), ExtentColor.RED));
		}
		
		String restaurant_details_book_button_expected = getObject("restaurant_details.book_button");
		
		if(restaurantDetailsPage.getTextForBookNowButton().equals(restaurant_details_book_button_expected)){
			logger.log(Status.PASS,MarkupHelper.createLabel( "Actual :"+restaurantDetailsPage.getTextForBookNowButton()+ " Expected :" +restaurant_details_book_button_expected , ExtentColor.GREEN));
			System.out.println("Actual :"+restaurantDetailsPage.getTextForBookNowButton()+ " Expected :" +restaurant_details_book_button_expected + ":PASS");
		}
		else{
			logger.log(Status.FAIL,MarkupHelper.createLabel( "text does not match for " +restaurantDetailsPage.getTextForBookNowButton() , ExtentColor.RED));
		}
			
		
		String tab_bar_my_reservations_expected = getObject("tab_bar.my_reservations");
		
		String tab_bar_search_expected = getObject("tab_bar.search");		
		
		String common_alert_button_no_expected = getObject("common_alert_button.no");

		String common_alert_button_ok_expected = getObject("common_alert_button.ok");

		String common_alert_button_yes_expected = getObject("common_alert_button.yes");

		String common_button_back_expected = getObject("common_button.back");

		String common_button_cancel_expected = getObject("common_button.cancel");

		String common_button_close_expected = getObject("common_button.close");

		String common_button_done_expected = getObject("common_button.done");

		String common_button_no_expected = getObject("common_button.no");

		String common_button_retry_expected = getObject("common_button.retry");

		String common_button_yes_expected = getObject("common_button.yes");

		String common_label_image_unavailable_text_expected = getObject("common_label.image_unavailable_text");

		String error_oops_text_expected = getObject("error.oops_text");

		String error_please_try_again_text_expected = getObject("error.please_try_again_text");

		String internet_error_offline_description_text_expected = getObject("internet_error.offline_description_text");

		String internet_error_offline_title_text_expected = getObject("internet_error.offline_description_text");

		String landingPage_iPad_landing_page_message_expected = getObject("landingPage_iPad.landing_page_message");



		String location_error_try_again_button_expected = getObject("location_error.try_again_button");


		String location_error_we_cant_find_your_location_text_expected = getObject("location_error.we_cant_find_your_location_text");

		String location_search_screen_suggestions_label_expected = getObject("location_search_screen.suggestions_label");

		String restaurant_details_average_title_expected = getObject("restaurant_details.average_title");

		String restaurant_details_main_course_title_expected = getObject("restaurant_details.main_course_title");

		String restaurant_details_total_food_spend_title_expected = getObject("restaurant_details.total_food_spend_title");

		String search_fetching_restaurants_title_expected = getObject("search.fetching_restaurants_title");

		String tab_bar_account_expected = getObject("tab_bar.account");

		String tab_bar_discover_expected = getObject("tab_bar.discover");
		
		String tab_bar_favourites_expected = getObject("tab_bar.favourites");

		String tab_bar_star_deals_expected = getObject("tab_bar.star_deals");

		String test_oops_text_expected = getObject("test.oops_text");

		String test_please_try_again_text_expected = getObject("test.please_try_again_text");
		
		String test_retry_button_text_expected = getObject("test.retry_button_text");
				
		System.out.println(common_alert_button_no_expected);

		System.out.println(common_alert_button_ok_expected);

		System.out.println(common_alert_button_yes_expected);

		System.out.println(common_button_back_expected);

		System.out.println(common_button_cancel_expected);

		System.out.println(common_button_close_expected);

		System.out.println(common_button_done_expected);

		System.out.println(common_button_no_expected);

		System.out.println(common_button_retry_expected);

		System.out.println(common_button_yes_expected);

		System.out.println(common_label_image_unavailable_text_expected);

		System.out.println(error_oops_text_expected);

		System.out.println(error_please_try_again_text_expected);

		System.out.println(internet_error_offline_description_text_expected);

		System.out.println(internet_error_offline_title_text_expected);

		System.out.println(landingPage_iPad_landing_page_message_expected);

		System.out.println(location_error_share_your_location_description_text_expected);

		System.out.println(location_error_share_your_location_title_text_expected);

		System.out.println(location_error_try_again_button_expected);

		System.out.println(location_error_turn_on_location_services_button_expected);

		System.out.println(location_error_we_cant_find_your_location_text_expected);

		System.out.println(location_search_screen_suggestions_label_expected);

		System.out.println(restaurant_details_average_title_expected);

		System.out.println(restaurant_details_main_course_title_expected);

		System.out.println(restaurant_details_total_food_spend_title_expected);

		System.out.println(search_fetching_restaurants_title_expected);

		System.out.println(tab_bar_account_expected);
		
		System.out.println(tab_bar_discover_expected);

		System.out.println(tab_bar_favourites_expected);

		System.out.println(tab_bar_my_reservations_expected);
		
		System.out.println(tab_bar_search_expected);
		
		System.out.println(tab_bar_star_deals_expected);

		System.out.println(test_oops_text_expected);

		System.out.println(test_please_try_again_text_expected);

		System.out.println(test_retry_button_text_expected);
	}

}
