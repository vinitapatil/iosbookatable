package com.BookATable;

import java.io.IOException;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.ScreenShot.utils.Utils;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;

public class ReportListener extends BaseTestInitializeClass implements  ISuiteListener, ITestListener {

	
	
	public void onStart(ISuite suite) {
		// TODO Auto-generated method stub
		
	}

	public void onFinish(ISuite suite) {
		// TODO Auto-generated method stub
		
	}

	public void onTestStart(ITestResult result) {
	}

	public void onTestSuccess(ITestResult result) {
		
		logger.log(Status.PASS, result.getName());
		
	}

	public void onTestFailure(ITestResult result) {
		Utils utils = new Utils();
		logger.log(Status.FAIL, "Test Case Failed is " + result.getName());
		logger.log(Status.FAIL, "Test Case Failed is " + result.getThrowable());
		try {
			logger.fail("Screen Shot : " + utils.takeScreenshot(driver, "Bookatable"));
			String screenShotPath = utils.takeScreenshot(driver, "Bookatable");
			
			logger.log(Status.FAIL, (Markup) logger.addScreenCaptureFromPath(screenShotPath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		
	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	

	
	

	


	
	

	
	

	
	

	

}
