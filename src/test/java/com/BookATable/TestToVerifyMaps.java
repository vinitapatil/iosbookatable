package com.BookATable;

import java.io.IOException;

import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class TestToVerifyMaps extends BaseTestInitializeClass{
		
	@Test(priority = 13)
	public void tapOnContactTab() throws IOException{
		logger = report.createTest("Restaurant Details Page: Tap On Contact Tab", "Verify functionality of Contact tab on Reataurant Details Page.");

		if (restaurantDetailsPage.tapOnContactTab()) {
			System.out.println("Tapped on Contact Tab on Reataurant Details Page.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Tapped on Contact Tab on Reataurant Details Page.", ExtentColor.GREEN));

		} else {
			System.out.println("Contact Tab was not found.");
			logger.log(Status.FAIL,
					MarkupHelper.createLabel("Failed to tap on Contact Tab as element was not found.", ExtentColor.RED));
			
		}
	}
	@Test(priority=14)
	public void tapOnMapViewToFullScreen() throws InterruptedException{
	//	logger = report.startTest("SearchPageTest");
		//logger.log(Status.INFO, "Test To Verify Maps");
		logger= report.createTest("Restaurant Details Page: tapOnMapViewToFullScreen", "Verify Map is open to full screen.");
		
		if(restaurantDetailsPage.tapOnMapView())
		{
			logger.log(Status.PASS, MarkupHelper.createLabel("Restaurant Details Page: Tapped on Map View from the Contact Tab of Restaurent Detail Page." , ExtentColor.GREEN));
			System.out.println("Tapped on Map View from the Contact Tab of Restaurent Detail Page.");
		}
		else
		{
			System.out.println("Map View could not be clicked.");			
			logger.log(Status.FAIL, MarkupHelper.createLabel("Map View could not be clicked", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}
	
	@Test(priority=15)
	public void tapOnMapBluePin() throws InterruptedException{
		logger= report.createTest("Map View: tapOnMapBluePin", "Verify Diner taps on map blue pin.");
		if(restaurantDetailsPage.tapOnMapViewBluePin())
		{
			logger.log(Status.PASS, MarkupHelper.createLabel("Blue Pin on Map view has been clicked." , ExtentColor.GREEN));
			System.out.println("Blue Pin on Map view has been clicked.");

		}
		else
		{
			System.out.println("Blue Pin on Map view could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Blue Pin on Map view could not be clicked.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}
	@Test(priority=16)
	public void getTextOnMapBluePin() throws InterruptedException{
		logger= report.createTest("Map View: getTextOnMapBluePin", "Verify text is displayed when diner taps on map blue pin.");

		if(restaurantDetailsPage.getTextOnMapView() != null)
		{
			logger.log(Status.PASS, MarkupHelper.createLabel("The Restaurent name is : "+restaurantDetailsPage.getTextOnMapView() , ExtentColor.GREEN));
			System.out.println("The Restaurent name is : "+restaurantDetailsPage.getTextOnMapView());

		}
		else
		{
			System.out.println("Text on Blue pin was not found.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Text on Blue pin was not found.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}
	@Test(priority=17)
	public void tapOnBookNowButton() throws InterruptedException{
		logger= report.createTest("Map View: tapOnBookNowButton", "Verify Book Now Button on Map view.");

		if(restaurantDetailsPage.tapOnBookNowButtonfunction())
		{
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Book Now Button" , ExtentColor.GREEN));
			System.out.println("Clicked on Book Now Button");

		}
		else
		{
			System.out.println("Book Now Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Book Now Button could not be clicked.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}
		@Test(priority=18)
		public void tapOnCloseBDAScreen() throws InterruptedException{
			logger= report.createTest("BDA Screen: tapOnCloseBDAScreen", "Verify Close button on BDA Screen.");
			Thread.sleep(10000);
		if(bdaScreen.tapOn_Close())
		{
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Close Button" , ExtentColor.GREEN));
			System.out.println("Clicked on Close Button.");

		}
		else
		{
			System.out.println("Close Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Close Button could not be clicked.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
		}
	@Test(priority=19)
	public void tapOnDoneButtonOnFullView() throws InterruptedException{
		logger= report.createTest("Map View: tapOnDoneButtonOnFullView", "Verify Done button on Map full view.");

		if(restaurantDetailsPage.tapOnDoneButton())
		{
			logger.log(Status.PASS, MarkupHelper.createLabel("DONE button has been clicked." , ExtentColor.GREEN));
			System.out.println("DONE button has been clicked.");

		}
		else
		{
			System.out.println("DONE button was not found.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("DONE button was not found.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
		//report.flush();
	}
	
	/*public void tapOnBackButtonOnDetailScreen() throws InterruptedException{
		logger= report.createTest("tapOnBackButtonOnDetailScreen", "Verify Back button on Details screen.");

		if(restaurantDetailsPage.tapOnBackButton())
		{
			logger.log(Status.PASS, MarkupHelper.createLabel("BACK button has been clicked." , ExtentColor.GREEN));
			System.out.println("BACK button has been clicked.");

		}
		else
		{
			System.out.println("BACK button was not found.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("BACK button was not found.", ExtentColor.RED));
			utils.takeScreenshot("Search", "SearchTest");
		}
		
		//report.endTest(logger);
		
	
	}*/
}
		

	

